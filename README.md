# gic-tfg
(Grado en Ingeniería de Computadores - Trabajo Final de Grado)
Object-oriented framework of face recognition


Generalización del software desarrollado en el proyecto "itis-pfc". 

Diseño e implementación en C++ de un framework Orientado a Objetos (OO) que permita a los investigadores en el área desarrollar diferentes versiones de sus algoritmos mediante la instanciación de unas pocas clases abstractas. El resultado será que el nuevo algoritmo podrá procesar vídeos, mostrar sus resultados, almacenar nuevos individuos en el almacén, etc. con poco esfuerzo.

Para desarrollar y probar el framework se han utilizado dos algoritmos de identificación facial:

1) El desarrollado en el anterior proyecto ("itis-pfc").
2) Un nuevo algoritmo que se basa en la Transformada Discreta del Coseno (DCT) y el Análisis Discriminante Lineal (LDA). El framework partirá de la premisa de que la identificación facial (descubrir qué individuo de entre los conocidos presentan las imágenes de una misma cara) se puede implementar utilizando diferentes algoritmos de verificación facial (dados dos conjuntos de imágenes de dos individuos establecer si representan la misma cara o no). 

El objetivo de desarrollo del framework se ha conseguido ya que es posible implementar dos algoritmos de identificación facial bastante diferentes dentro del mismo. Por tanto, el software podrá utilizarse en pruebas e investigación sobre algoritmos de identificación facial. 

