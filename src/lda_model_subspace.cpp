
// -----------------------------------------------------------------------------
/**
 *  @brief ...
 *  @author Jose M. Buenaposada, Pablo Marín
 *  @date 2013/5
 *  @version $revision$
 *
 *  $id$
 *
 *  Grupo de investigación en Percepción Computacional y Robótica)
 *  (Perception for Computers & Robots research Group)
 *  Facultad de Informática (Computer Science School)
 *  Universidad Politécnica de Madrid (UPM) (Madrid Technical University)
 *  http://www.dia.fi.upm.es/~pcr
 *
 */
// -----------------------------------------------------------------------------

#include "lda_model_subspace.hpp"
//#include "lda_projection.hpp"
//#include "lda_pca_projection.hpp"
#include "pca_lda_pie_model.h" // Changed by JMBUENA.

namespace upm { namespace pcr
{

// -----------------------------------------------------------------------------
//
// Purpose and Method:  
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
LDAModelSubspace::LDAModelSubspace
  ()
{
  m_P    = cv::Mat(ROWS_PCA_LDA_P_MATRIX, COLS_PCA_LDA_P_MATRIX, cv::DataType<float>::type, PCA_LDA_P_MATRIX);
  m_mean = cv::Mat(ROWS_PCA_LDA_MEAN_MATRIX, COLS_PCA_LDA_MEAN_MATRIX, cv::DataType<float>::type, PCA_LDA_MEAN_MATRIX);
}

// -----------------------------------------------------------------------------
//
// Purpose and Method:  
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
LDAModelSubspace::~LDAModelSubspace
  ()
{
}
 
// -----------------------------------------------------------------------------
//
// Purpose and Method:  
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
cv::Mat 
LDAModelSubspace::computeLowLevelDescriptor
  (
  cv::Mat image
  )
{
  //------------------------------------------------------------------
  // Project the input image vector over the LDA model subspace.
  cv::Mat image_vector;
  image.reshape(1, image.rows * image.cols).convertTo(image_vector, m_mean.type());
  cv::Mat lda = m_P * (image_vector - m_mean);
  
  return lda;
}

  
}; }; // namespace

