
// -----------------------------------------------------------------------------
/**
 *  @brief AllFacesTracker interface definition.
 *  @author Jose M. Buenaposada, Pablo L. Marín
 *  @date 2014/03/03
 *  @version $revision$
 *
 *  $id$
 *
 *  Grupo de investigación en Percepción Computacional y Robótica)
 *  (Perception for Computers & Robots research Group)
 *  Facultad de Informática (Computer Science School)
 *  Universidad Politécnica de Madrid (UPM) (Madrid Technical University)
 *  http://www.dia.fi.upm.es/~pcr
 *
 */
// -----------------------------------------------------------------------------

// ------------------ RECURSION PROTECTION -------------------------------------
#ifndef WRITE_IMAGES_HPP
#define WRITE_IMAGES_HPP

// ----------------------- INCLUDES --------------------------------------------
#include <cxcore.hpp>
#include <highgui.hpp>
//#include <opencv2/opencv.hpp>
#include <sstream>
#include <string>
#include "trace.hpp"

namespace upm { namespace pcr
{
// ----------------------------------------------------------------------------
/**
 * @class WriteImages
 * @brief Save an array of cv::Mat images
 */
// -----------------------------------------------------------------------------
class WriteImages
{
#define FOLDER "/home/pablitomarin/Documentos/Universidad/3/PFC/Codigo_C++/face_analysis_framework_pfcs/trunk/src/images/"

public:
    
  WriteImages
  (){};
  
  ~WriteImages
  (){};
  
  /**
   * @brief Save images and return true if success
   *
   * @param prefix Images name prefix.
   * @param images Images to save.
   */
  static bool
  write
  (
   std::string prefix,
   std::vector<cv::Mat> images,
   bool default_folder = true
  )
  {
   
    bool success = true;
    int size = images.size();
    
    for(int i = 0; i < size; i++)
    {
      std::stringstream aux;
      aux << prefix << "x" << i;
      if(! write(aux.str(), images[i]))
      {
	success = false;
	break;
      }
    }
    
    return success;
  };
  
  
  static bool
  write
  (
   long prefix,
   std::vector<cv::Mat> images,
   bool default_folder = true
  )
  {
    bool success = true;
    int size = images.size();
    
    for(int i = 0; i < size; i++)
    {
      std::stringstream aux;
      aux << prefix << "x" << i;
      if(! write(aux.str(), images[i]))
      {
	success = false;
	break;
      }
    }
    
    return success;
  };
  
  /**
   * @brief Save image and return true if success
   *
   * @param prefix Images name prefix.
   * @param images Images to save.
   */
  static bool
  write
  (
   std::string prefix,
   cv::Mat image,
   bool default_folder = true
  )
  {
    bool success = true;
    
    if(default_folder)
    {
      std::stringstream aux;
      aux << prefix << ".png";
      if(! cv::imwrite(aux.str(), image))
      {
        success = false;
      } 
    }
    else
    {
      std::stringstream aux;
      aux << FOLDER << prefix << ".png";
      if(! cv::imwrite(aux.str(), image))
      {
        success = false;
      }
    }
    
    return success;
  };
  
  
  static bool
  write
  (
   long prefix,
   cv::Mat image,
   bool default_folder = true
  )
  {
    bool success = true;
    
    if(default_folder)
    {
      std::stringstream aux;
      aux << prefix << ".png";
      if(! cv::imwrite(aux.str(), image))
      {
        success = false;
      }
    }
    else
    {
      std::stringstream aux;
      aux << FOLDER << prefix << ".png";
        
      if(! cv::imwrite(aux.str(), image))
      {
        success = false;
      }
    }
    
    return success;
  };
  
  
private:
  
  
  
};

}; }; // namespace

#endif // WRITE_IMAGES_HPP
