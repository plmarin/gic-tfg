// -----------------------------------------------------------------------------
/**
 *  @brief Image region processor interface definition
 *  @author Jose M. Buenaposada
 *  @date 2009/11/29
 *  @version $revision$
 *
 *  $id$
 *
 *  Grupo de investigación en Percepción Computacional y Robótica)
 *  (Perception for Computers & Robots research Group)
 *  Facultad de Informática (Computer Science School)
 *  Universidad Politécnica de Madrid (UPM) (Madrid Technical University)
 *  http://www.dia.fi.upm.es/~pcr
 *
 */
// -----------------------------------------------------------------------------

// ------------------ RECURSION PROTECTION -------------------------------------
#ifndef IMAGE_REGION_PROCESSOR_HPP
#define IMAGE_REGION_PROCESSOR_HPP

// ----------------------- INCLUDES --------------------------------------------
#include <boost/shared_ptr.hpp>
#include "viewer.hpp"
#include "face_attribute.hpp"
// #include "face.hpp"
#include <vector>

namespace upm { namespace pcr
{
// ----------------------------------------------------------------------------
/**
 *  Definition of an smart pointer to FaceProcessor type
 */
// -----------------------------------------------------------------------------
class Face;
class FaceProcessor;
typedef boost::shared_ptr<FaceProcessor> FaceProcessorPtr;

// -----------------------------------------------------------------------------
/**
 *  Definition of a container of smart pointers to FaceProcessor
 */
// -----------------------------------------------------------------------------
typedef std::vector<FaceProcessorPtr> FaceProcessors;

// ----------------------------------------------------------------------------
/**
 * @class FaceProcessor
 * @brief A class that defines the interface for a given face analysis.
 */
// -----------------------------------------------------------------------------
class FaceProcessor
{
public:

  FaceProcessor
    () {};

  virtual ~FaceProcessor
    () {};

  /**
   * @brief Process an interesting face
   *
   * @param  pFrame Image comming from the camera.
   * @param  f The face within pFrame to process.
   */
  virtual void
  processFrame 
   (
   IplImage* pFrame,
   Face& f 
   ) = 0;

  /**
   * @brief Shows the result of face processing on a given Viewer.
   *
   * @param pViewer The viewer over to which display results.
   * @param pFrame Last processed camera frame.
   * @param f The Face processed in the last call to "processFrame".
   */
  virtual void
  showResults
   (
   Viewer* pViewer,
   IplImage* pFrame,
   Face& f
   ) = 0;

  /**
   * @brief Returns a pointer to a copy of itself.
   */
  virtual FaceProcessorPtr
  clone
   () = 0;

  /**
   * @brief Get the attributes computed by the image region processor.
   *
   * @param attributes
   */
  virtual void
  getAttributes
    (
    FaceAttributes& attributes
    ) = 0;
};

}; }; // namespace

#endif
