// -----------------------------------------------------------------------------
/**
 *  @brief Implementation of face similarity warping image region processor.
 *  @author Jose M. Buenaposada
 *  @date 2011/06/13
 *  @version $revision
 *
 *  $id$
 *
 *  Grupo de investigación en Percepción Computacional y Robótica)
 *  (Perception for Computers & Robots research Group)
 *  Facultad de Informática (Computer Science School)
 *  Universidad Politécnica de Madrid (UPM) (Madrid Technical University)
 *  http://www.dia.fi.upm.es/~pcr
 *
 */
// -----------------------------------------------------------------------------

#include "similarity_warp_filter.hpp"
#include <opencv/cv.h>
#include "trace.hpp"

namespace upm { namespace pcr {

// -----------------------------------------------------------------------------
//
// Purpose and Method:
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
SimilarityWarpFilter::SimilarityWarpFilter 
  ( 
  CvPoint src_point1,
  CvPoint src_point2,
  CvPoint dst_point1,
  CvPoint dst_point2,
  CvSize dst_img_size
  ):
  m_src_point1(src_point1), 
  m_src_point2(src_point2), 
  m_dst_point1(dst_point1), 
  m_dst_point2(dst_point2)
{
  m_dst_img_size.width = dst_img_size.width;
  m_dst_img_size.height = dst_img_size.height;
}

// -----------------------------------------------------------------------------
//
// Purpose and Method:  Copy constructor
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
SimilarityWarpFilter::SimilarityWarpFilter 
( 
  const SimilarityWarpFilter& filter 
) 
{
  (*this) = (filter);
}

// -----------------------------------------------------------------------------
//
// Purpose and Method:  Asignment operator.
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
SimilarityWarpFilter& 
SimilarityWarpFilter::operator= 
  ( 
  const SimilarityWarpFilter& filter 
  )
{
  if (&filter == this)
  {
    return (*this);
  };
  
  return (*this);
}

// -----------------------------------------------------------------------------
//
// Purpose and Method:
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
SimilarityWarpFilter::~SimilarityWarpFilter()
{
}

// -----------------------------------------------------------------------------
//
// Purpose and Method: 
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
SimilarityWarpFilterPtr
SimilarityWarpFilter::clone
  ()
{
  SimilarityWarpFilterPtr filter_ptr(new SimilarityWarpFilter((*this)));

  return filter_ptr;
}

// -----------------------------------------------------------------------------
//
// Purpose and Method:
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
void
SimilarityWarpFilter::operator() 
  ( 
  IplImage* pFrame, 
  Face& f,
  IplImage* pFilteredImage
  )
{
  int x, y;
  int width, height;
  ImageRegion region;
  double dist_src, dist_dst;
  double scale;
  double inv_scale;
//   int img_width, img_height;
  double final_angle, init_angle;
  
  assert(pFrame != NULL);
  assert(pFilteredImage != NULL);

  f.getRegion(region);
  region.getParams(x, y, width, height);
  
  dist_src   = sqrt(static_cast<double>(((m_src_point2.x - m_src_point1.x) *
                                         (m_src_point2.x - m_src_point1.x)) + 
                                        ((m_src_point2.y - m_src_point1.y) * 
                                         (m_src_point2.y - m_src_point1.y)) )); 
  dist_dst   = sqrt(static_cast<double>(((m_dst_point2.x - m_dst_point1.x) * 
                                         (m_dst_point2.x - m_dst_point1.x)) + 
                                        ((m_dst_point2.y - m_dst_point1.y) * 
                                         (m_dst_point2.y - m_dst_point1.y)) )); 
  scale      = dist_src / dist_dst;    
  
  // We compute the similarity transform that transform the line between src_point1 and
  // src_point2 to the line between dst_point1 and dst_point2.
  CvMat* pM   = cvCreateMat(2, 3, CV_32FC1);
  CvMat* pT1  = cvCreateMat(3, 3, CV_32FC1);
  CvMat* pT2  = cvCreateMat(2, 3, CV_32FC1);
  CvMat* pR1  = cvCreateMat(3, 3, CV_32FC1);
  CvMat* pR2  = cvCreateMat(3, 3, CV_32FC1);
  CvMat* pS   = cvCreateMat(3, 3, CV_32FC1);
  CvMat* pAux = cvCreateMat(3, 3, CV_32FC1);
    
  CvPoint2D32f midpoint_src = cvPoint2D32f( 
    static_cast<double>(m_src_point2.x + m_src_point1.x)/2.0 + x, 
    static_cast<double>(m_src_point2.y + m_src_point1.y)/2.0 + y );
  CvPoint2D32f midpoint_dst = cvPoint2D32f( 
    static_cast<double>(m_dst_point2.x + m_dst_point1.x)/2.0, 
    static_cast<double>(m_dst_point2.y + m_dst_point1.y)/2.0 );

  init_angle         = 0.0; 
  final_angle        = std::atan(static_cast<double>(m_src_point2.y - m_src_point1.y) / 
                                 static_cast<double>(m_src_point2.x - m_src_point1.x));

  cvSetIdentity(pR1, cvRealScalar(1.0));
  cvmSet(pR1, 0, 0,  cos(-init_angle));
  cvmSet(pR1, 0, 1, -sin(-init_angle));
  cvmSet(pR1, 1, 0,  sin(-init_angle));
  cvmSet(pR1, 1, 1,  cos(-init_angle));

  cvSetIdentity(pT1, cvRealScalar(1.0));
  cvmSet(pT1, 0, 2, -midpoint_dst.x); 
  cvmSet(pT1, 1, 2, -midpoint_dst.y); 

  cvSetIdentity(pS, cvRealScalar(scale));
  cvmSet(pS, 2, 2, 1.0);

  cvSetIdentity(pR2, cvRealScalar(1.0));
  cvmSet(pR2, 0, 0,  cos(final_angle));
  cvmSet(pR2, 0, 1, -sin(final_angle));
  cvmSet(pR2, 1, 0,  sin(final_angle));
  cvmSet(pR2, 1, 1,  cos(final_angle));
  
  cvSetIdentity(pT2, cvRealScalar(1.0));
  cvmSet(pT2, 0, 2, midpoint_src.x) ;
  cvmSet(pT2, 1, 2, midpoint_src.y);
  
  // M = T2*R2*S*R1*T1; R2 is the identity matrix
  cvMatMul( pR1, pT1, pAux );
  cvMatMul( pS, pAux, pAux );
  cvMatMul( pR2, pAux, pAux );
  cvMatMul( pT2, pAux, pM );

  cvWarpAffine( pFrame, pFilteredImage, pM, CV_WARP_INVERSE_MAP );

  cvReleaseMat(&pM); 
  cvReleaseMat(&pT1); 
  cvReleaseMat(&pT2); 
  cvReleaseMat(&pR1); 
  cvReleaseMat(&pR2); 
  cvReleaseMat(&pS); 
  cvReleaseMat(&pAux); 
};

}; }; // namespaces
