// -----------------------------------------------------------------------------
/**
 *  @brief ImageRegion interface definition.
 *  @author Jose M. Buenaposada
 *  @date 2009/11/29
 *  @version $revision$
 *
 *  $id$
 *
 *  Grupo de investigación en Percepción Computacional y Robótica)
 *  (Perception for Computers & Robots research Group)
 *  Facultad de Informática (Computer Science School)
 *  Universidad Politécnica de Madrid (UPM) (Madrid Technical University)
 *  http://www.dia.fi.upm.es/~pcr
 *
 */
// -----------------------------------------------------------------------------

// ------------------ RECURSION PROTECTION -------------------------------------
#ifndef IMAGE_REGION_HPP
#define IMAGE_REGION_HPP

// ----------------------- INCLUDES --------------------------------------------
#include <boost/shared_ptr.hpp>
//#include <vector>
//#include <face_region_processors_manager.hpp>

namespace upm { namespace pcr
{
// ----------------------------------------------------------------------------
/**
 *  Definition of an smart pointer to Face object
 */
// -----------------------------------------------------------------------------
class ImageRegion;
typedef boost::shared_ptr<ImageRegion> ImageRegionPtr;

// -----------------------------------------------------------------------------
/**
 * @class ImageRegion
 * @brief A class that stores information about an image subregion.
 */
// -----------------------------------------------------------------------------
class ImageRegion
{
public:

  ImageRegion
    ():
    m_x(0),
    m_y(0),
    m_width(0),
    m_height(0)
  {};

  ImageRegion
    (
    int x,
    int y,
    int width,
    int height
    ):
    m_x(x),
    m_y(y),
    m_width(width),
    m_height(height)
  {};

  ~ImageRegion
    () {};

  /**
   * @brief Returns the region parameters.
   *
   * @param  x horizontal coordinate of top left corner in pixels.
   * @param  y vertical coordinate of top right corner in pixels.
   * @param  width area width in pixels
   * @param  height area  height in pixels
   */
  void
  getParams
   (
   int& x,
   int& y,
   int& width,
   int& height
   ) {x = m_x; y = m_y; width = m_width; height = m_height; };

  /**
   * @brief Changes the region parameters.
   *
   * @param  x horizontal coordinate of top left corner in pixels.
   * @param  y vertical coordinate of top right corner in pixels.
   * @param  width area width in pixels
   * @param  height area height in pixels
   */
  void
  setParams
   (
   int x,
   int y,
   int width,
   int height
   ) {m_x = x; m_y = y; m_width = width; m_height = height; };

protected:
  int m_x;
  int m_y;
  int m_width;
  int m_height;
};

}; }; // namespace

#endif
