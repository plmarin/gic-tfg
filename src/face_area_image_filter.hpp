// -----------------------------------------------------------------------------
/**
 *  @brief Face area image filter interface definition
 *  @author Jose M. Buenaposada
 *  @date 2011/01/29
 *  @version $revision$
 *
 *  $id$
 *
 *  Grupo de investigación en Percepción Computacional y Robótica)
 *  (Perception for Computers & Robots research Group)
 *  Facultad de Informática (Computer Science School)
 *  Universidad Politécnica de Madrid (UPM) (Madrid Technical University)
 *  http://www.dia.fi.upm.es/~pcr
 *
 */
// -----------------------------------------------------------------------------

// ------------------ RECURSION PROTECTION -------------------------------------
#ifndef FACE_AREA_IMAGE_FILTER_HPP
#define FACE_AREA_IMAGE_FILTER_HPP

// ----------------------- INCLUDES --------------------------------------------
#include <boost/shared_ptr.hpp>
#include "face.hpp"
#include <vector>
#include <opencv/cv.h>

namespace upm { namespace pcr
{
// ----------------------------------------------------------------------------
/**
 *  Definition of an smart pointer to FaceAreaImageFilter type
 */
// -----------------------------------------------------------------------------
class FaceAreaImageFilter;
typedef boost::shared_ptr<FaceAreaImageFilter> FaceAreaImageFilterPtr;

// -----------------------------------------------------------------------------
/**
 *  Definition of a container of smart pointers to FaceAreaImageFilter
 */
// -----------------------------------------------------------------------------
typedef std::vector<FaceAreaImageFilterPtr> FaceAreaImageFilters;

// ----------------------------------------------------------------------------
/**
 * @class FaceAreaImageFilter
 * @brief Defines the interface for filtering the face area image.
 */
// -----------------------------------------------------------------------------
class FaceAreaImageFilter
{
public:

  FaceAreaImageFilter
    () {};

  virtual ~FaceAreaImageFilter
    () {};

  /**
   * @brief Process an interesting image region 
   *
   * @param  pFrame Image comming from the camera.
   * @param  f The face on pFrame to process.
   * @return pFilterdImage The filtered image. 
   */
  virtual void
  operator ()
   (
   IplImage* pFrame,
   Face& f,
   IplImage* pFilteredImage
   ) = 0;
};

}; }; // namespace

#endif
