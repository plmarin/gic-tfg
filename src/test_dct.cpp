
#include <opencv/cv.h>
#include "dct_blocks_extractor_jm.hpp"
#include "pie_imagen_prueba.hpp"
#include "dct_pca_lda_mean.hpp"
#include "dct_pca_lda_model.hpp"
#include <cmath>
#include <iostream>

int 
main
  ( 
  int argc, 
  char** argv 
  )
{
  cv::Mat image;
  upm::pcr::DCTBlocksExtractorPtr dct(new upm::pcr::DCTBlocksExtractor());
  upm::pcr::Face f(0);
  cv::Point l = cv::Point(14, 17); //11, 15);
  cv::Point r = cv::Point(46, 14); //47, 18);
  upm::pcr::ImageRegion imageRegion = upm::pcr::ImageRegion(0, 0, 64, 64);
  
  f.setRegion(imageRegion);
  f.setEyesRelativeCoordinates(l, r);


  cv::Mat extractedFeatures = cv::Mat(1125, 1, cv::DataType<float>::type);

  image = cv::imread("prueba.jpeg", CV_LOAD_IMAGE_GRAYSCALE);
  //image = cv::imread("/home/pablitomarin/Documentos/Universidad/3/PFC/Codigo_C++/face_analysis_framework_pfcs/trunk/src/images/0_x_20.png", CV_LOAD_IMAGE_GRAYSCALE);
  
  /*cv::namedWindow("Display window", CV_WINDOW_AUTOSIZE);
  cv::imshow("Display window", image);

  cv::waitKey(0);*/ //Saca bien la imagen
  
  dct->operator()(image, f, extractedFeatures);
  //dct(image, f, extractedFeatures);
  
  //std::cout << "Vector Length: " << dct->getFeatureVectorLength() << std::endl; //320
   
  cv::Mat m_PCA_LDA_MEAN_MATRIX(ROWS_PCA_LDA_MEAN_MATRIX, COLS_PCA_LDA_MEAN_MATRIX, CV_32FC1, PCA_LDA_MEAN_MATRIX);
  cv::Mat m_PCA_LDA_P_MATRIX(ROWS_PCA_LDA_P_MATRIX, COLS_PCA_LDA_P_MATRIX, CV_32FC1, PCA_LDA_P_MATRIX);
  
  cv::Mat pPCA_LDAVector(m_PCA_LDA_P_MATRIX.rows, extractedFeatures.cols, CV_32FC1);
  std::cout << "pPCA_LDAVector.rows: " << pPCA_LDAVector.rows << std::endl;
  std::cout << "pPCA_LDAVector.cols: " << pPCA_LDAVector.cols << std::endl;
  std::cout << "extractedFeatures.rows: " << extractedFeatures.rows << std::endl; //1125
  std::cout << "extractedFeatures.cols: " << extractedFeatures.cols << std::endl; //1
  std::cout << "m_PCA_LDA_P_MATRIX.rows: " << m_PCA_LDA_P_MATRIX.rows << std::endl; //1125
  std::cout << "m_PCA_LDA_P_MATRIX.cols: " << m_PCA_LDA_P_MATRIX.cols << std::endl; //1
  std::cout << "m_PCA_LDA_MEAN_MATRIX.rows: " << m_PCA_LDA_MEAN_MATRIX.rows << std::endl; //1125
  std::cout << "m_PCA_LDA_MEAN_MATRIX.cols: " << m_PCA_LDA_MEAN_MATRIX.cols << std::endl; //1
  pPCA_LDAVector = m_PCA_LDA_P_MATRIX * (extractedFeatures - m_PCA_LDA_MEAN_MATRIX);
  
  for(int i = 0; i < pPCA_LDAVector.rows; i++)
  {
    for(int j = 0; j < pPCA_LDAVector.cols; j++)
    {
      // Es de tamaño 1x1
      std::cout << "pPCA_LDAVector(" << i << "," << j << ") -> " << pPCA_LDAVector.at<float>(i,j) << std::endl;
    }
  }
  
  float sum = 0.0;
  for(int i = 0; i < extractedFeatures.rows; i++) 
  {
    for(int j = 0; j < extractedFeatures.cols; j++)
    {
      std::cout << "extractedFeatures(i,j) = (" << i << "," << j << ") -> " << extractedFeatures.at<float>(i,j) << std::endl;
      sum += extractedFeatures.at<float>(i,j);
    }
  }
  std::cout << "SUM = " << sum << std::endl;
  
  sum = 0.0;
  std::cout << "m_PCA_LDA_MEAN_MATRIX: " << std::endl;
  for(int i = 0; i < m_PCA_LDA_MEAN_MATRIX.rows; i++) 
  {
    for(int j = 0; j < m_PCA_LDA_MEAN_MATRIX.cols; j++)
    {
      std::cout << "(i,j) = (" << i << "," << j << ") -> " << m_PCA_LDA_MEAN_MATRIX.at<float>(i,j) << std::endl;
      sum += m_PCA_LDA_MEAN_MATRIX.at<float>(i,j);
    }
  }
  std::cout << "SUM = " << sum << std::endl;
  
  sum = 0.0;
  std::cout << "m_PCA_LDA_P_MATRIX: " << std::endl;
  for(int i = 0; i < m_PCA_LDA_P_MATRIX.rows; i++) 
  {
    for(int j = 0; j < m_PCA_LDA_P_MATRIX.cols; j++)
    {
      std::cout << "(i,j) = (" << i << "," << j << ") -> " << m_PCA_LDA_P_MATRIX.at<float>(i,j) << std::endl;
      sum += m_PCA_LDA_P_MATRIX.at<float>(i,j);
    }
  }
  std::cout << "SUM = " << sum << std::endl;
  
  cv::Mat mult = cv::Mat::ones(1,1125,cv::DataType<float>::type);
  cv::Mat aux_mult = mult * extractedFeatures;
  std::cout << "extractedFeatures --> " << aux_mult.at<float>(0,0) << std::endl;
  
  return 0;
}