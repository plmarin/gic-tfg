// -----------------------------------------------------------------------------
/**
 *  @brief EyesDetector implementation
 *  @author Jose M. Buenaposada#include "face_processor.hpp"

 *  @date 2011/01/26
 *  @version $revision$
 *
 *  $id$
 *
 *  Grupo de investigación en Percepción Computacional y Robótica)
 *  (Perception for Computers & Robots research Group)
 *  Facultad de Informática (Computer Science School)
 *  Universidad Politécnica de Madrid (UPM) (Madrid Technical University)
 *  http://www.dia.fi.upm.es/~pcr
 *
 */
// -----------------------------------------------------------------------------

// ----------------------- INCLUDES --------------------------------------------
#include "eyes_detector.hpp"
#include "image_region.hpp"
#include <opencv/cv.h>
#include "trace.hpp"
#include <limits>
#include <cmath>

#define ROUND(number) ((number < 0.0) ? ceil(number - 0.5) : floor(number + 0.5))

namespace upm { namespace pcr
{
  
#define MIN_NUM_NEIGHTBOURS 3
#define SCALE_STEP 1.1f
#define SCALE_REDUCTION 0.3

// -----------------------------------------------------------------------------
//
// Purpose and Method: Constructor
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
EyesDetector::EyesDetector
  (
  CvHaarClassifierCascade* pCascadeLeft,
  CvHaarClassifierCascade* pCascadeRight,
  int& orig_face_window_size
  )
{
  assert(pCascadeLeft != NULL);
  assert(pCascadeRight != NULL);
  
  m_pStorage              = cvCreateMemStorage(0);
  m_pCascadeLeft          = pCascadeLeft;
  m_pCascadeRight         = pCascadeRight;

  m_orig_face_window_size = orig_face_window_size;
  
};

// -----------------------------------------------------------------------------
//
// Purpose and Method: Destructor
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
EyesDetector::~EyesDetector
  ()
{
  if (m_pStorage) 
  {
    cvReleaseMemStorage(&m_pStorage);
  }

  if (m_pCascadeLeft) 
  {
    cvReleaseHaarClassifierCascade(&m_pCascadeLeft);
  };

  if (m_pCascadeRight) 
  {
    cvReleaseHaarClassifierCascade(&m_pCascadeRight);
  };
};

// -----------------------------------------------------------------------------
//
// Purpose and Method: 
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
void
EyesDetector::detect
  (
  IplImage* pFrame,
  ImageRegion& region, 
  CvPoint& left_eye,
  CvPoint& right_eye
  )
{
  double face_scale;
  int x, y, width, height;
  CvPoint offset;
  
  assert(m_pCascadeRight != NULL);
  assert(m_pCascadeLeft != NULL);
  assert(m_pStorage != NULL);

  // Set the Region of Interest to estimate the coordinates of the left eye
  region.getParams(x, y, width, height);

  // We compute the face scale for eyes detection minimum window size.
  face_scale = SCALE_REDUCTION*static_cast<double>(width)/m_orig_face_window_size;

  // Look for the left eye ...
  //cvSetImageROI(pFrame, cvRect(x, y + ROUND(static_cast<float>(height)/5.0), 
  cvSetImageROI(pFrame, cvRect(x, y + ROUND(static_cast<float>(height)/4.0), 
	                              ROUND(static_cast<float>(width)/2.0) , 
				      ROUND(static_cast<float>(height)/2.0) ) );
				      //ROUND(static_cast<float>(height)/2.5) ) );
  offset.x  = 0;
  //offset.y  = ROUND(static_cast<float>(height)/5.0);
  offset.y  = ROUND(static_cast<float>(height)/4.0);
  detectSingleEye(pFrame, region, left_eye, offset, m_pCascadeLeft, face_scale);

  // Look for the right eye ...
  cvSetImageROI(pFrame, cvRect(x + width/2.0, 
                               //y + (height/5.0), 
                               y + (height/4.0), 
                               width/2.0, 
                               height/2.0));
                               //height/2.5));
  offset.x  = ROUND(static_cast<float>(width)/2.0);
  //offset.y  = ROUND(static_cast<float>(height)/5.0);
  offset.y  = ROUND(static_cast<float>(height)/4.0);
  detectSingleEye(pFrame, region, right_eye, offset, m_pCascadeRight, face_scale);
};


// -----------------------------------------------------------------------------
//
// Purpose and Method: 
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
void
EyesDetector::detectSingleEye
  (
  IplImage* pFrame,
  ImageRegion& region, 
  CvPoint& eye_pos,
  CvPoint& offset,
  CvHaarClassifierCascade* pCascade,
  double face_scale
  )
{
//   double face_scale;
  int x_eye, y_eye;
  double min_distance;
  int x_min, y_min;
  int x_r, y_r;

  // detect left eye 
  CvSeq* eyes = cvHaarDetectObjects( 
                pFrame, pCascade, m_pStorage,
                SCALE_STEP, MIN_NUM_NEIGHTBOURS, 0, 
                cvSize(ROUND(static_cast<float>(pCascade->orig_window_size.width)*face_scale), 
                       ROUND(static_cast<float>(pCascade->orig_window_size.height)*face_scale) ) );

  x_eye = -1;
  y_eye = -1;
  if ((eye_pos.x != -1) && (eye_pos.y != -1))
  {
    // If we have a previous location for the eye ( != -1)
    // we look for the detected eye closer to the previous 
    // detection.
    double min_distance = std::numeric_limits<double>::max();
    x_min     = -1;
    y_min     = -1;
    for(int i = 0; i < (eyes ? eyes->total : 0); i++ ) 
    {
      CvRect* r = (CvRect*)cvGetSeqElem( eyes, i );
      x_r       = r->x + ROUND(static_cast<float>(r->width)/2.0);
      y_r       = r->y + ROUND(static_cast<float>(r->height)/2.0);
      double distance = (x_r*x_r)+(y_r*y_r);
      if (distance < min_distance)
      {
        min_distance = distance;
        x_min        = x_r;
        y_min        = y_r;
      }
    }
    x_eye = x_min;
    y_eye = y_min;
  }
  else if (eyes->total > 0)
  {
    // If we have detected an eye and we have no previous location
    // for the eye ( = -1) we choose the first detection ...
    CvRect* r  = (CvRect*)cvGetSeqElem( eyes, 0 );
    x_eye = r->x + r->width/2; 
    y_eye = r->y + r->height/2; 
  }
    
  cvResetImageROI(pFrame);

  if ((x_eye != -1) && (y_eye != -1))
  {
    x_eye += offset.x;
    y_eye += offset.y;
  }

  // reset buffer for the next object detection 
  cvClearMemStorage(m_pStorage);
  
  eye_pos.x = x_eye;
  eye_pos.y = y_eye;
  eye_pos.x = x_eye;
  eye_pos.y = y_eye;
};


// -----------------------------------------------------------------------------
//
// Purpose and Method: 
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
void 
EyesDetector::showResults
  (
  Viewer* pViewer,
  IplImage *pFrame,
  ImageRegion region, 
  CvPoint left_eye,
  CvPoint right_eye
  )
{
  int x, y, width, height;
  float color_left[]  = {1., 1., 0.};
  float color_right[] = {1., 0., 1.};
  
  // Get the full face image region
  region.getParams(x, y, width, height);

  if ((left_eye.x >= 0) && (left_eye.y >= 0))    
  {
    // Draw a cross over the eye in the left part of the image.
    pViewer->line(x + left_eye.x - 10, y + left_eye.y,
                  x + left_eye.x + 10, y + left_eye.y, 2, color_left);
    pViewer->line(x + left_eye.x, y + left_eye.y - 10,
                  x + left_eye.x, y + left_eye.y + 10, 2, color_left);
  }

  if ((right_eye.x >= 0) && (right_eye.y >= 0))    
  {  
    // Draw a cross over the eye in the right part of the image.
    pViewer->line(x + right_eye.x - 10, y + right_eye.y,
                  x + right_eye.x + 10, y + right_eye.y, 2, color_right);
    pViewer->line(x + right_eye.x, y + right_eye.y - 10,
                  x + right_eye.x, y + right_eye.y + 10, 2, color_right);
  }  
};

}; }; // namespace
