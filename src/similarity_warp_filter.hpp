// -----------------------------------------------------------------------------
/**
 *  @brief Similariry warp image region filter interface definition
 *  @author Jose M. Buenaposada
 *  @date 2011/06/13
 *  @version $revision$
 *
 *  $id$
 *
 *  Grupo de investigación en Percepción Computacional y Robótica)
 *  (Perception for Computers & Robots research Group)
 *  Facultad de Informática (Computer Science School)
 *  Universidad Politécnica de Madrid (UPM) (Madrid Technical University)
 *  http://www.dia.fi.upm.es/~pcr
 *
 */
// -----------------------------------------------------------------------------

// ------------------ RECURSION PROTECTION -------------------------------------
#ifndef SIMILARIRY_WARP_FILTER_HPP
#define SIMILARIRY_WARP_FILTER_HPP

// ----------------------- INCLUDES --------------------------------------------
#include "face_area_image_filter.hpp"
#include <boost/shared_ptr.hpp>
#include <opencv/cv.h>
#include <opencv/ml.h>

namespace upm { namespace pcr
{
// ----------------------------------------------------------------------------
/**
 *  Definition of an smart pointer to SimilarityWarpFilter type
 */
// -----------------------------------------------------------------------------
class SimilarityWarpFilter;
typedef boost::shared_ptr<SimilarityWarpFilter> SimilarityWarpFilterPtr;

// ----------------------------------------------------------------------------
/**
 * @class SimilarityWarpFilter
 * @brief An image region filter for equalization and masking
 */
// -----------------------------------------------------------------------------
class SimilarityWarpFilter: public FaceAreaImageFilter
{
public:

  SimilarityWarpFilter
    (
    CvPoint src_point1,
    CvPoint src_point2,
    CvPoint dst_point1,
    CvPoint dst_point2,
    CvSize dst_img_size
    );  
    
  /** Copy constructor */
  SimilarityWarpFilter
    (
    const SimilarityWarpFilter& filter
    );
    
  void
  setDstPoints
    (
    CvPoint& point1,
    CvPoint& point2
    ) { m_dst_point1 = point1; m_dst_point2 = point2; };

  void
  setSrcPoints
    (
    CvPoint& point1,
    CvPoint& point2
    ) { m_src_point1 = point1;  m_src_point2 = point2; };
    
  /** Asignment operator */
  SimilarityWarpFilter&
  operator =
    (
    const SimilarityWarpFilter& filter
    );    

  ~SimilarityWarpFilter
    ();

  SimilarityWarpFilterPtr
  clone
    ();
    
  /**
   * @brief Crops the face region from the input image.
   *
   * @param pFrame Image comming from the camera.
   * @param f The face within pFrame to process.
   * @param pFilteredImage The output of the procedure
   */
  virtual void
  operator () 
   (
   IplImage* pFrame,
   Face& f,
   IplImage* pFilteredImage
   );
   
private:

  CvPoint m_src_point1;
  CvPoint m_src_point2;
  CvPoint m_dst_point1;
  CvPoint m_dst_point2;
  CvSize  m_dst_img_size;
};

}; }; // namespace

#endif
