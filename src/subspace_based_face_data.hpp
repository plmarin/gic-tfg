// -----------------------------------------------------------------------------
/**
 *  @brief ...
 *  @author Jose M. Buenaposada, Pablo Marín
 *  @date 2013/5
 *  @version $revision$
 *
 *  $id$
 *
 *  Grupo de investigación en Percepción Computacional y Robótica)
 *  (Perception for Computers & Robots research Group)
 *  Facultad de Informática (Computer Science School)
 *  Universidad Politécnica de Madrid (UPM) (Madrid Technical University)
 *  http://www.dia.fi.upm.es/~pcr
 *
 */
// -----------------------------------------------------------------------------

// ------------------ RECURSION PROTECTION -------------------------------------
#ifndef SUBSPACE_BASED_FACE_DATA_HPP
#define SUBSPACE_BASED_FACE_DATA_HPP

// ----------------------- INCLUDES --------------------------------------------
#include "face_data.hpp"

namespace upm { namespace pcr
{

// ----------------------------------------------------------------------------
/**
 * @class SubspaceBasedFaceData
 * @brief ...
 */
// -----------------------------------------------------------------------------

class SubspaceBasedFaceData: public FaceData
{
public:
  
  SubspaceBasedFaceData
    ();
  
  virtual 
  ~SubspaceBasedFaceData
    ();
  
  /**
   * @brief Compare two descriptors set
   *
   * @param new_descriptors
   * @param db_descriptors
   */
  virtual cv::Mat
  compare
    (
    //SubspaceBasedFaceData& new_descriptors,
    //SubspaceBasedFaceData& db_descriptors
    FaceDataPtr new_descriptors,
    FaceDataPtr db_descriptors
    );
    
  /**
   * @brief
   *
   * @param image
   */   
  virtual void
  addImage
    (
    cv::Mat image
    );
    
  /**
   * @brief If there are enough images return true
   *
   * @param 
   */
  virtual bool
  isOk
    ();
  
};

}; }; // namespace

#endif // SUBSPACE_BASED_FACE_DATA_HPP
