// -----------------------------------------------------------------------------
/**
 *  @brief ...
 *  @author Jose M. Buenaposada, Pablo Marín
 *  @date 2013/5
 *  @version $revision$
 *
 *  $id$
 *
 *  Grupo de investigación en Percepción Computacional y Robótica)
 *  (Perception for Computers & Robots research Group)
 *  Facultad de Informática (Computer Science School)
 *  Universidad Politécnica de Madrid (UPM) (Madrid Technical University)
 *  http://www.dia.fi.upm.es/~pcr
 *
 */
// -----------------------------------------------------------------------------

#include "permutations_comparator.hpp"
#include <limits>

namespace upm { namespace pcr
{
  
// -----------------------------------------------------------------------------
//
// Purpose and Method:  
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// ----------------------------------------------------------------------------- 
PermutationsComparator::PermutationsComparator
  () : DescriptorComparator()
{
}

// -----------------------------------------------------------------------------
//
// Purpose and Method:  
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
PermutationsComparator::~PermutationsComparator
  ()
{
}

// -----------------------------------------------------------------------------
//
// Purpose and Method:  
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
double 
PermutationsComparator::compare
  (
  cv::Mat new_descriptor,
  cv::Mat descriptor
  )
{
  // Find similarity of "descriptor" to the "db_subject-th" individual in the DB
  double similarity = 0;
  for (int j = 0; j < descriptor.rows; j++)  
  {
    similarity += std::max(new_descriptor.rows - new_descriptor.at<int>(j,0), 0) *
		  std::max(descriptor.rows - descriptor.at<int>(j,0), 0);
  }
  
  return similarity;
}

// -----------------------------------------------------------------------------
//
// Purpose and Method:  
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
bool 
PermutationsComparator::isDistance
  ()
{
  return false;
}

// -----------------------------------------------------------------------------
//
// Purpose and Method:  
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
bool 
PermutationsComparator::isSimilarity
  ()
{
  return true;
}

}; }; // namespace

