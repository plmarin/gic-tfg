// -----------------------------------------------------------------------------
/**
 *  @brief Face interface definition.
 *  @author Jose M. Buenaposada
 *  @date 2009/13/10
 *  @version $revision$
 *
 *  $id$
 *
 *  Grupo de investigación en Percepción Computacional y Robótica)
 *  (Perception for Computers & Robots research Group)
 *  Facultad de Informática (Computer Science School)
 *  Universidad Politécnica de Madrid (UPM) (Madrid Technical University)
 *  http://www.dia.fi.upm.es/~pcr
 *
 */
// -----------------------------------------------------------------------------

// ------------------ RECURSION PROTECTION -------------------------------------
#ifndef FACE_HPP
#define FACE_HPP

// ----------------------- INCLUDES --------------------------------------------
#include <boost/shared_ptr.hpp>
#include <vector>
#include "face_processor.hpp"
#include "image_region.hpp"
#include <opencv/cv.h>

namespace upm { namespace pcr
{
// ----------------------------------------------------------------------------
/**
 *  Definition of an smart pointer to Face object
 */
// -----------------------------------------------------------------------------
class Face;
typedef boost::shared_ptr<Face> FacePtr;

// -----------------------------------------------------------------------------
/**
 *  Definition of a container of smart pointers to Face objects
 */
// -----------------------------------------------------------------------------
typedef std::vector<FacePtr> Faces;

// -----------------------------------------------------------------------------
/**
 * @class Face
 * @brief A class that stores information about the state of a known face
 */
// -----------------------------------------------------------------------------
class Face
{
public:

  Face
    (long int face_id): m_region(0, 0, 0, 0), m_face_id(face_id)  {};

  ~Face
    () {};

  long int 
  getFaceId() { return m_face_id; };

  void
  setFaceId(long int face_id) { m_face_id = face_id; };

  /**
   * @brief Returns the last known position of the face.
   *
   * @param region An ImageRegion object with the current face position.
   */
  void
  getRegion
    (
    ImageRegion& region
    ) { region = m_region; };

  /**
   * @brief Returns the last known position of the eyes.
   *
   * Bear in mind that here left and right are image concepts. The left_eye_pos are 
   * the coordinates of the eye appearing in the image pn the left.
   *
   * @param left_eye Pixel coordinates relative to the top left corner of the face image region.
   * @param right_eye Pixel coordinates relative to the top left corner of the face image region.
   */
  void
  getEyesRelativeCoordinates
    (
    CvPoint& left_eye,
    CvPoint& right_eye
    ) { left_eye = m_left_eye_coords; right_eye = m_right_eye_coords; };

  /**
   * @brief Updates the last known position of the eyes.
   *
   * Bear in mind that here left and right are image concepts. The left_eye_pos are 
   * the coordinates of the eye appearing in the image pn the left.
   *
   * @param left_eye Pixel coordinates relative to the top left corner of the face image region.
   * @param right_eye Pixel coordinates relative to the top left corner of the face image region.
   */
  void
  setEyesRelativeCoordinates
    (
    CvPoint left_eye,
    CvPoint right_eye
    ) { m_left_eye_coords = left_eye; m_right_eye_coords = right_eye; };

    
  /**
   * @brief Returns the last known position of the nose
   *
   * @param nose Pixel coordinates relative to the top left corner of the face image region.
   */
  void
  getNoseRelativeCoordinates
    (
    CvPoint& nose
    ) { nose = m_nose_coords; };
        
  /**
   * @brief Updates the last known position of the nose
   *
   * @param nose Pixel coordinates relative to the top left corner of the face image region.
   */
  void
  setNoseRelativeCoordinates
    (
    CvPoint nose
    ) { m_nose_coords = nose; };

  /**
   * @brief Returns the last known position of the mouth
   *
   * @param mouth Pixel coordinates relative to the top left corner of the face image region.
   */
  void
  getMouthRelativeCoordinates
    (
    CvPoint& mouth
    ) { mouth = m_mouth_coords; };    
    
  /**
   * @brief Updates the last known position of the mouth
   *
   * @param mouth Pixel coordinates relative to the top left corner of the face image region.
   */
  void
  setMouthRelativeCoordinates
    (
    CvPoint mouth
    ) { m_mouth_coords = mouth; };
    
  /**
   * @brief Updates the known position of the face.
   *
   * @param region An ImageRegion object with the new face position.
   */
  void
  setRegion
    (
    ImageRegion& region
    ) { m_region = region; };

  /**
   * @brief Sets the region processor to process this face on each frame.
   *
   * @param processor_ptr Region processor for the face (can be a group).
   */
  void
  setProcessor
    (
    FaceProcessorPtr processor_ptr
    ) {  m_processor_ptr = processor_ptr; };

  /**
   * @brief Returns true after at least one call to setProcessors with a valid pointer.
   */
  bool
  hasProcessor
    () { return (m_processor_ptr.use_count() > 0);  };

  /**
   * @brief Calls all face region processors over the face image region.
   * 
   * @param  pFrame Image comming from the camera.
   */
  void
  processFaceRegion
    (
    IplImage* pFrame
    );

  /**
   * @brief Shows the result all region processors over the face image region.
   *
   * @param pViewer The viewer for displaying results.
   * @param pFrame Last processed camera frame.
   */
  void
  showResults
    (
    Viewer* pViewer,
    IplImage* pFrame
    );

  /**
   * @brief Get the face attributes computed by the image region processors.
   *
   * @param attributes
   */
  void
  getAttributes
    (
    FaceAttributes& attributes
    );

private:
  ImageRegion m_region;
  FaceProcessorPtr m_processor_ptr;
  CvPoint m_left_eye_coords;
  CvPoint m_right_eye_coords;
  CvPoint m_nose_coords;
  CvPoint m_mouth_coords;

  long int m_face_id;
};

}; }; // namespace

#endif
