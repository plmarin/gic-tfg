// -----------------------------------------------------------------------------
/**
 *  @brief ...
 *  @author Jose M. Buenaposada, Pablo Marín
 *  @date 2013/5
 *  @version $revision$
 *
 *  $id$
 *
 *  Grupo de investigación en Percepción Computacional y Robótica)
 *  (Perception for Computers & Robots research Group)
 *  Facultad de Informática (Computer Science School)
 *  Universidad Politécnica de Madrid (UPM) (Madrid Technical University)
 *  http://www.dia.fi.upm.es/~pcr
 *
 */
// -----------------------------------------------------------------------------

// ------------------ RECURSION PROTECTION -------------------------------------
#ifndef FACE_DATA_HPP
#define FACE_DATA_HPP

// ----------------------- INCLUDES --------------------------------------------
#include <boost/shared_ptr.hpp>
#include <opencv/cv.h>
#include <opencv/ml.h>
#include <queue>

namespace upm { namespace pcr
{

class FaceData;
typedef boost::shared_ptr<FaceData> FaceDataPtr;

// ----------------------------------------------------------------------------
/**
 * @class FaceData
 * @brief Abstract base class for...
 *
 */
// -----------------------------------------------------------------------------

class FaceData
{
public:
   
  FaceData
    () {};
  
  virtual 
  ~FaceData
    () {};
    
  virtual FaceDataPtr
  clone
    () = 0;  
   
  /**
   * @brief Compare two descriptors set
   *
   * @param new_descriptors
   * @param db_descriptors
   */
  virtual cv::Mat
  //virtual float
  compare
    (
     FaceDataPtr new_descriptors,
     FaceDataPtr db_descriptors
    ) = 0;
    
  /**
   * @brief
   *
   * @param image
   */   
  virtual void
  addImage
    (
    cv::Mat image
    ) = 0;
  
  /**
   * @brief If there are enough images return true
   *
   * @param 
   */
  virtual bool
  isOk
    () = 0;
  
};


}; }; // namespace

#endif // FACE_DATA_HPP
