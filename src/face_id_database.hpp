// -----------------------------------------------------------------------------
/**
 *  @brief Face recognition Data Base class
 *  @author Jose M. Buenaposada, Pablo Marín
 *  @date 2012/4
 *  @version $revision$
 *
 *  $id$
 *
 *  Grupo de investigación en Percepción Computacional y Robótica)
 *  (Perception for Computers & Robots research Group)
 *  Facultad de Informática (Computer Science School)
 *  Universidad Politécnica de Madrid (UPM) (Madrid Technical University)
 *  http://www.dia.fi.upm.es/~pcr
 *
 */
// -----------------------------------------------------------------------------

// ------------------ RECURSION PROTECTION -------------------------------------
#ifndef FACE_ID_DATA_BASE_HPP
#define FACE_ID_DATA_BASE_HPP

// ----------------------- INCLUDES --------------------------------------------
//#include <opencv/cv.h>
//#include <opencv/ml.h>
#include <queue>
#include <limits>
#include <boost/shared_ptr.hpp>
#include <boost/smart_ptr.hpp>
#include "viewer.hpp"
#include "face_data.hpp"


namespace upm { namespace pcr
{

class FaceIdDataBase;
typedef boost::shared_ptr<FaceIdDataBase> FaceIdDataBasePtr;    
  
// ----------------------------------------------------------------------------
/**
 * @class FaceIdDataBase
 * @brief Face Descriptors Data Base and face recogniser
 * 
 * In this class we compare descriptors using a Distance subclass object. 
 * The distance between descriptors once computed is used to 
 */
// -----------------------------------------------------------------------------
class FaceIdDataBase
{
public:
  /** This is the type for face identifiers */
  typedef unsigned int id_type; 
  static const id_type UNKNOWN_ID;
  static const id_type MAX_ID;
  
  /** Data Base type */
  typedef std::vector<FaceDataPtr> vectorDataBase;
  
  /** Static (class) method to convert a face identifier to std::string */
  static std::string faceIdToString(id_type face_id);

  FaceIdDataBase
    ( 
    FaceDataPtr faceData,
    const double std_same_id,
    const double mean_same_id,
    const double std_different_id,
    const double mean_different_id
//    const bool multiply_probability_density = true
    );  

  ~FaceIdDataBase
    ();

  /**
   * @brief Process the descriptors of a face for recognition.
   *
   * @param descriptors The queue of descriptors from a face.
   */
  id_type
  getID
   (
   FaceDataPtr descriptors,
   cv::Mat representative_image
   );

  void
  showDataBaseImages
    (
    Viewer* pViewer,
    int images_per_row = 5
    );


private:
  vectorDataBase m_dataBase;
  FaceDataPtr m_faceData;
  // We store a representative image per individual in the database.
  std::vector<cv::Mat> m_images; 
  
  const double m_std_same_id;
  const double m_mean_same_id;
  const double m_std_different_id;
  const double m_mean_different_id;
  const double m_max_prob_same_for_new;
  const double m_diff_prob_same_for_unknown;
  const double m_same_prior;
  const double m_diff_prior;
//  const bool m_multiply_probability_density;
  
  double 
  findDescriptorSimilarityToSubject
    (
    int db_subject, 
    cv::Mat descriptor    
    );

  void 
  normpdf_similarity
    (
    double x,
    double& prob_same, 
    double& prob_diff
    );

  void
  log_normpdf_similarity
    (
    double x,
    double& prob_same,
    double& prob_diff
    );

  double
  normpdf_same
    (
    double x
    );
    
  double
  normpdf_diff
    (
    double x
    );

  double
  log_normpdf_same
    (
    double x
    );

  double
  log_normpdf_diff
    (
    double x
    );


//  void
//  multiply_probability_density
//    (
//    double& prob,
//    double prob_id
//    );
  
//  void
//  add_probability_density
//    (
//    double& prob,
//    double prob_id
//    );
  
};

}; }; // namespace

#endif

