// -----------------------------------------------------------------------------
/**
 *  @brief Implementation of face processor.
 *  @author Jose M. Buenaposada, Pablo Marín
 *  @date 2012/04
 *  @version $revision$
 *
 *  $id$
 *
 *  Grupo de investigación en Percepción Computacional y Robótica)
 *  (Perception for Computers & Robots research Group)
 *  Facultad de Informática (Computer Science School)
 *  Universidad Politécnica de Madrid (UPM) (Madrid Technical University)
 *  http://www.dia.fi.upm.es/~pcr
 *
 */
// -----------------------------------------------------------------------------
#include "face_id_processor.hpp"
#include "face.hpp"
#include <sstream>
#include "trace.hpp"
#include "img_properties.hpp"
#include "similarity_warp_filter.hpp"
#include "write_images.hpp"


namespace upm { namespace pcr
{

const int FACE_CROPPED_WIDTH                  = IMG_WIDTH; //64;
const int FACE_CROPPED_HEIGHT                 = IMG_HEIGHT; //64;
const float DETECTION_WINDOW_REDUCTION_TOP    = 0.20;
const float DETECTION_WINDOW_REDUCTION_LEFT   = 0.15;
const float DETECTION_WINDOW_REDUCTION_RIGHT  = 0.15;
const float DETECTION_WINDOW_REDUCTION_BOTTOM = 0.1;

static unsigned int image_count = 0;

// For eye detection face rectification
#define NORM_LEFT_EYE_X 7
#define NORM_LEFT_EYE_Y 10
#define NORM_RIGHT_EYE_X 16
#define NORM_RIGHT_EYE_Y 10
#define NORM_FACE_SIZE 25

// Vector max size to save images
#define MAX_SIZE 25
  
// -----------------------------------------------------------------------------
//
// Purpose and Method:  
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
FaceIdProcessor::FaceIdProcessor
  (
  FaceDataPtr face_data,
  FaceIdDataBasePtr face_id_db,
  bool use_eyes
  ):
  m_face_id_db(face_id_db),
  m_use_eyes(use_eyes)
{
  m_image     = cv::Mat::zeros(FACE_CROPPED_WIDTH, FACE_CROPPED_HEIGHT, cv::DataType<uint8_t>::type);
  m_face_id   = FaceIdDataBase::UNKNOWN_ID;
  m_face_data = face_data->clone();

  double scale         = (static_cast<double>(FACE_CROPPED_WIDTH) / static_cast<double>(NORM_FACE_SIZE));
  m_norm_left_eye.x    = round(NORM_LEFT_EYE_X * scale);
  m_norm_left_eye.y    = round(NORM_LEFT_EYE_Y * scale);
  m_norm_right_eye.x   = round(NORM_RIGHT_EYE_X * scale);
  m_norm_right_eye.y   = round(NORM_RIGHT_EYE_Y * scale);
}

// -----------------------------------------------------------------------------
//
// Purpose and Method:  Copy constructor
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
FaceIdProcessor::FaceIdProcessor
  (
  const FaceIdProcessor& processor
  )
{
  (*this) = (processor);
}

// -----------------------------------------------------------------------------
//
// Purpose and Method:  Asignment operator.
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
FaceIdProcessor&
FaceIdProcessor::operator =
  (
  const FaceIdProcessor& processor
  )
{   
  if (&processor == this)
  {
    return (*this);
  };

  m_image  = processor.m_image.clone();

  m_face_id    = processor.m_face_id;
  m_face_data  = processor.m_face_data->clone();
  m_face_id_db = processor.m_face_id_db;

  m_use_eyes         = processor.m_use_eyes;
  m_norm_left_eye.x  = processor.m_norm_left_eye.x;
  m_norm_left_eye.y  = processor.m_norm_left_eye.y;
  m_norm_right_eye.x = processor.m_norm_right_eye.x;
  m_norm_right_eye.y = processor.m_norm_right_eye.y;

//  // Those are only a reference to the constant data matrix.
//  m_P    = processor.m_P;
//  m_mean = processor.m_mean;
  
  m_first_images = processor.m_first_images;
  m_last_images  = processor.m_last_images;
  
  return (*this);  
}

// -----------------------------------------------------------------------------
//
// Purpose and Method:  
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
FaceIdProcessor::~FaceIdProcessor
  ()
{  
/*  std::stringstream prefix_first, prefix_last;
  prefix_first << image_count << "_first_" << m_face_id;
  prefix_last << image_count << "_last_" << m_face_id;
  
  if(! WriteImages::write(prefix_first.str(), m_first_images, false))
    std::cout << "Fallo al escribir m_fist_images" << std::endl;
  else
    std::cout << "Escrito un vector de imágenes (first)..." << std::endl;
  
  if(! WriteImages::write(prefix_last.str(), m_last_images, false))
    std::cout << "Fallo al escribir m_last_images" << std::endl;
  else
    std::cout << "Escrito un vector de imágenes (last)..." << std::endl;
  
  image_count++;*/
}

// -----------------------------------------------------------------------------
//
// Purpose and Method: 
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
FaceProcessorPtr
FaceIdProcessor::clone
  ()
{
  FaceProcessorPtr processor_ptr(dynamic_cast<FaceProcessor*>(new FaceIdProcessor((*this))));

  return processor_ptr;
}

// -----------------------------------------------------------------------------
//
// Purpose and Method: 
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
void
FaceIdProcessor::processFrame 
 (
 IplImage* pFrame,
 Face& f
 )
{
  ImageRegion region;
  int x, y, width, height;
  int x_crop, y_crop, width_crop, height_crop;
  CvPoint left_eye, right_eye;
  cv::Mat aux_image;

  cv::Mat frame(pFrame, false);

  f.getRegion(region);
  region.getParams(x, y, width, height);

  if (!m_use_eyes)
  {
    x_crop = x + round(width * DETECTION_WINDOW_REDUCTION_LEFT);
    y_crop = y + round(height * DETECTION_WINDOW_REDUCTION_TOP);
    height_crop = round(height * (1.0 - (DETECTION_WINDOW_REDUCTION_TOP + DETECTION_WINDOW_REDUCTION_BOTTOM)));
    width_crop = round(width * (1.0 - (DETECTION_WINDOW_REDUCTION_LEFT + DETECTION_WINDOW_REDUCTION_RIGHT)));

    //------------------------------------------------------------------
    // Crop the face from the input image.
    cv::Mat roi(frame, cv::Rect(x_crop, y_crop, width_crop, height_crop)); 
    cv::resize(roi, aux_image, cv::Size(FACE_CROPPED_WIDTH, FACE_CROPPED_HEIGHT));
  }
  else // if (m_use_eyes)
  {
    f.getEyesRelativeCoordinates(left_eye, right_eye);

    // If we have not detected both eyes we do not process the frame
    if ((left_eye.x  == -1) || (left_eye.y  == -1) ||
        (right_eye.x == -1) || (right_eye.y == -1))
    {
      return;
    }
    std::cout << "left_eye=" << left_eye.x << "," << left_eye.y << std::endl;
    std::cout << "right_eye=" << right_eye.x << "," << right_eye.y << std::endl;
    std::cout << "m_norm_left_eye=" << m_norm_left_eye.x << "," << m_norm_left_eye.y << std::endl;
    std::cout << "m_norm_right_eye=" << m_norm_right_eye.x << "," << m_norm_right_eye.y << std::endl;
    std::cout << "=======================" << std::endl;
    SimilarityWarpFilter warpFilter(left_eye, right_eye,
                                    m_norm_left_eye, m_norm_right_eye,
                                    cvSize(FACE_CROPPED_WIDTH, FACE_CROPPED_HEIGHT));


    if (frame.channels() == 3)
    {
      aux_image = cv::Mat::zeros(FACE_CROPPED_HEIGHT, FACE_CROPPED_WIDTH, cv::DataType<cv::Vec<uchar, 3> >::type);
    }
    else // frame.channels() == 1
    {
      aux_image = cv::Mat::zeros(FACE_CROPPED_HEIGHT, FACE_CROPPED_WIDTH, cv::DataType<uchar>::type);
    }
    IplImage aux_image_iplimage = aux_image;
    IplImage frame_iplimage     = frame;
    warpFilter(&frame_iplimage, f, &aux_image_iplimage);

  } // if (use_eyes)

  // Convert the cropped image to gray scale
  if (frame.channels() == 3)
  {
    cv::cvtColor(aux_image, m_image, CV_BGR2GRAY);
  }
  else
  {
    m_image = aux_image.clone();
  }

  //------------------------------------------------------------------
  // Add image to the FaceData
  m_face_data->addImage(m_image);

  //------------------------------------------------------------------
  // Save images
  //this->saveImages();
//  this->saveImage();
  
  //------------------------------------------------------------------
  // If the FaceData has enough information compute the Id
  if (m_face_data->isOk())
  {
    m_face_id = m_face_id_db->getID(m_face_data, m_image.clone());
  }
}

// -----------------------------------------------------------------------------
//
// Purpose and Method: 
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
void
FaceIdProcessor::showResults
  (
  Viewer* pViewer,
  IplImage* pFrame,
  Face& f
  )
{
  int x_window, y_window;
  int window_width, window_height;
  float color[] = {1.0, 1.0, 1.0};
  float black[] = {0.0, 0.0, 0.0};
  std::ostringstream out;
  ImageRegion region;

  // Get the image region from the face.
  f.getRegion(region);

  region.getParams(x_window, y_window, window_width, window_height);

  // Plot ID:
  int rectangle_height = floor(static_cast<double>(window_width) * 0.2);
  int plot_area_border = floor(static_cast<double>(rectangle_height) * 0.15);
  int plot_area_left = x_window; // + window_width + plot_area_border;

  pViewer->filled_rectangle(x_window, y_window - rectangle_height,
                            window_width, rectangle_height, black);

  out << std::setprecision(3);
  out << "Id = "<< FaceIdDataBase::faceIdToString(m_face_id);
  pViewer->text(out.str(), 
                x_window, 
                y_window - plot_area_border, 
                color, 
                static_cast<double>(window_width) / 250.0,
                1);

  
  // Plot cropped image.
  IplImage ipl_image = m_image;
  pViewer->image(&ipl_image, x_window + window_width, 
                 y_window + 20, window_width * 0.5, window_width * 0.5);
};

// -----------------------------------------------------------------------------
//
// Purpose and Method: 
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
void
FaceIdProcessor::getAttributes
  (
  FaceAttributes& attributes
  )
{
  bool expression_attr_found = false;
  std::string attr_name = "face_id";

  // Male probability
  FaceAttributePtr attribute_face_id_ptr(new FaceAttribute(attr_name, 
                                 FaceAttribute::ATTR_STRING_TYPE,
                                 FaceIdDataBase::faceIdToString(m_face_id),
                                 m_face_id_confidence));
  attributes.insert(std::make_pair(attr_name, attribute_face_id_ptr));
};

// -----------------------------------------------------------------------------
//
// Purpose and Method: 
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
void
FaceIdProcessor::saveImages
 ()
{  
  // Descomentar destructor
/*
  if(m_first_images.size() < MAX_SIZE)
  {
    m_first_images.push_back(m_image);
  }
  else
  {
    if(m_last_images.size() >= MAX_SIZE)
    {
      m_last_images.pop_back();
    }
    m_last_images.push_back(m_image);
  }
*/
}

// -----------------------------------------------------------------------------
//
// Purpose and Method: 
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
void
FaceIdProcessor::saveImage
 ()
{
  // Comentar destructor
  
  std::stringstream prefix;
  prefix << m_face_id << "_x_" << image_count;

  WriteImages::write(prefix.str(), m_image, false);
  
  image_count++;
}


}; }; // namespace

