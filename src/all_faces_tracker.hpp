// -----------------------------------------------------------------------------
/**
 *  @brief AllFacesTracker interface definition.
 *  @author Jose M. Buenaposada
 *  @date 2011/05/17
 *  @version $revision$
 *
 *  $id$
 *
 *  Grupo de investigación en Percepción Computacional y Robótica)
 *  (Perception for Computers & Robots research Group)
 *  Facultad de Informática (Computer Science School)
 *  Universidad Politécnica de Madrid (UPM) (Madrid Technical University)
 *  http://www.dia.fi.upm.es/~pcr
 *
 */
// -----------------------------------------------------------------------------

// ------------------ RECURSION PROTECTION -------------------------------------
#ifndef ALL_FACES_TRACKER_HPP
#define ALL_FACES_TRACKER_HPP

// ----------------------- INCLUDES --------------------------------------------
#include "viewer.hpp"
#include "face.hpp"
#include "faces_tracker.hpp"
#include "eyes_detector.hpp"

namespace upm { namespace pcr
{
// ----------------------------------------------------------------------------
/**
 * @class AllFacesTracker
 * @brief Detects the biggest face in the image frame (the nearest to camera).
 */
// -----------------------------------------------------------------------------
class AllFacesTracker: public FacesTracker
{
public:
  
  AllFacesTracker
    (
    CvHaarClassifierCascade* pCascade,
    CvHaarClassifierCascade* pCascadeLeftEye,
    CvHaarClassifierCascade* pCascadeRightEye,
    double scale_step,
    double min_neighbors,
    double min_face_size,
    bool draw_eyes = true
    );

  ~AllFacesTracker
    ();

  /**
   * @brief Detect and track all the faces in image.
   *
   * @param pImage an IplImage OpenCv image.
   * @param tracked_faces Container with the biggest face found.
   */
  void 
  processFrame 
    (
    IplImage *pImage,
    Faces& tracked_faces
    );

  /**
   * @brief Plot results after a processFrame call.
   *
   * @param pViewer The "Graphic Window" over to which draw the results.
   * @param pImage The last processed frame by a "processFrame" call.
   * @param tracked_faces Container with the faces found in "processFrame" call.
   */
  void 
  showResults
    (
    Viewer* pViewer,
    IplImage *pImage,
    Faces& tracked_faces
    );

  /**
   * @brief Gets the list of detected faces
   */
/*
  void
  getTrackedFaces
    (
    Faces& tracked_faces
    );
*/

private:

  CvMemStorage* m_pStorage;
  CvHaarClassifierCascade* m_pCascade;
  upm::pcr::EyesDetector m_eyes_detector;
  
 // Faces m_tracked_faces;
  double m_scale_step;
  int m_min_neighbors;
  int m_min_face_size;
  bool m_draw_eyes;
  long int m_num_faces_created;
};

}; }; // namespace

#endif
