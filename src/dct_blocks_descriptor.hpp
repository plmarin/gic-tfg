// -----------------------------------------------------------------------------
/**
 *  @brief ...
 *  @author Jose M. Buenaposada, Pablo Marín
 *  @date 2013/5
 *  @version $revision$
 *
 *  $id$
 *
 *  Grupo de investigación en Percepción Computacional y Robótica)
 *  (Perception for Computers & Robots research Group)
 *  Facultad de Informática (Computer Science School)
 *  Universidad Politécnica de Madrid (UPM) (Madrid Technical University)
 *  http://www.dia.fi.upm.es/~pcr
 *
 */
// -----------------------------------------------------------------------------

// ------------------ RECURSION PROTECTION -------------------------------------
#ifndef DCT_BLOCKS_DESCRIPTOR_HPP
#define DCT_BLOCKS_DESCRIPTOR_HPP

// ----------------------- INCLUDES --------------------------------------------
#include "image_descriptor.hpp"
#include "face_area_image_extractor.hpp"
#include <boost/shared_ptr.hpp>
#include <opencv/cv.h>
#include <opencv/ml.h>
#include <stdexcept>

namespace upm { namespace pcr
{
  
// ----------------------------------------------------------------------------
/*!
 *  @class EyesNotFound
 *  @brief 
 */
// ----------------------------------------------------------------------------
class EyesNotFoundException: public std::logic_error
{
public:
  EyesNotFoundException(const std::string& what_arg): logic_error(what_arg) {};
};  


// ----------------------------------------------------------------------------
/**
 *  Definition of an smart pointer to DCTBlocksDescriptor type
 */
// -----------------------------------------------------------------------------
class DCTBlocksDescriptor;
typedef boost::shared_ptr<DCTBlocksDescriptor> DCTBlocksDescriptorPtr;

// ----------------------------------------------------------------------------
/**
 * @class DCTBlocksDescriptor
 * @brief An image region filter for equalization and masking
 *
 * See paper:
 *  J. Stallkamp, H. K. Ekenel, R. Stiefelhagen 
 *  Video-based Face Recognition on Real-World Data 
 *  Int. Conference on Computer Vision - ICCV'07, 
 *  Rio de Janeiro, Brasil, October 2007. 
 *
 */
// -----------------------------------------------------------------------------

class DCTBlocksDescriptor : public ImageDescriptor
{
public:
  
  DCTBlocksDescriptor
    ();
    
  virtual 
  ~DCTBlocksDescriptor
    ();
    
   /** Copy constructor */
  DCTBlocksDescriptor
    (
    const DCTBlocksDescriptor& descriptor
    );

  /** Asignment operator */
  DCTBlocksDescriptor&
  operator =
    (
    const DCTBlocksDescriptor& descriptor
    );

  DCTBlocksDescriptorPtr
  clone
    ();  
  
  /**
   * @brief Compute the subjects distances based high level descriptor
   */
  virtual cv::Mat
  computeHighLevelDescriptor
    (
    cv::Mat low_level_descriptor
    );
  
private:
  
  const unsigned int m_STEP;
  
};

}; }; // namespace





#endif // DCT_BLOCKS_DESCRIPTOR_HPP
