// -----------------------------------------------------------------------------
/**
 *  @brief ...
 *  @author Jose M. Buenaposada, Pablo Marín
 *  @date 2013/5
 *  @version $revision$
 *
 *  $id$
 *
 *  Grupo de investigación en Percepción Computacional y Robótica)
 *  (Perception for Computers & Robots research Group)
 *  Facultad de Informática (Computer Science School)
 *  Universidad Politécnica de Madrid (UPM) (Madrid Technical University)
 *  http://www.dia.fi.upm.es/~pcr
 *
 */
// -----------------------------------------------------------------------------

#include "l1_comparator.hpp"
#include <limits>

namespace upm { namespace pcr
{
  
// -----------------------------------------------------------------------------
//
// Purpose and Method:  
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// ----------------------------------------------------------------------------- 
L1Comparator::L1Comparator
  () : DescriptorComparator()
{
}

// -----------------------------------------------------------------------------
//
// Purpose and Method:  
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
L1Comparator::~L1Comparator
  ()
{
}

// -----------------------------------------------------------------------------
//
// Purpose and Method:  
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
double 
L1Comparator::compare
  (
  cv::Mat new_descriptor,
  cv::Mat descriptor
  )
{
  // Find similarity of "descriptor" to the "db_subject-th" individual in the DB
  double distance = 0;
  for (int j = 0; j < descriptor.rows; j++)  
  {
    distance += std::abs<int>(descriptor.at<int>(j, 0) - new_descriptor.at<int>(j, 0));
  }
    
  return distance;
}

// -----------------------------------------------------------------------------
//
// Purpose and Method:  
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
bool 
L1Comparator::isDistance
  ()
{
  return true;
}

// -----------------------------------------------------------------------------
//
// Purpose and Method:  
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
bool 
L1Comparator::isSimilarity
  ()
{
  return false;
}

}; }; // namespace



