
// -----------------------------------------------------------------------------
/**
 *  @brief ...
 *  @author Jose M. Buenaposada, Pablo Marín
 *  @date 2013/5
 *  @version $revision$
 *
 *  $id$
 *
 *  Grupo de investigación en Percepción Computacional y Robótica)
 *  (Perception for Computers & Robots research Group)
 *  Facultad de Informática (Computer Science School)
 *  Universidad Politécnica de Madrid (UPM) (Madrid Technical University)
 *  http://www.dia.fi.upm.es/~pcr
 *
 */
// -----------------------------------------------------------------------------

#include "dct_comparator.hpp"
#include <limits>

namespace upm { namespace pcr
{

#include "dct_pca_lda_mean.hpp"
#include "dct_pca_lda_model.hpp"

// -----------------------------------------------------------------------------
//
// Purpose and Method:  
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// ----------------------------------------------------------------------------- 
DCTComparator::DCTComparator
  () : DescriptorComparator()
{
  m_P    = cv::Mat(ROWS_PCA_LDA_P_MATRIX, COLS_PCA_LDA_P_MATRIX, cv::DataType<float>::type, PCA_LDA_P_MATRIX);
  m_mean = cv::Mat(ROWS_PCA_LDA_MEAN_MATRIX, COLS_PCA_LDA_MEAN_MATRIX, cv::DataType<float>::type, PCA_LDA_MEAN_MATRIX);  
}

// -----------------------------------------------------------------------------
//
// Purpose and Method:  
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
DCTComparator::~DCTComparator
  ()
{
}

// -----------------------------------------------------------------------------
//
// Purpose and Method:  
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
double 
DCTComparator::compare
  (
  cv::Mat new_descriptor,
  cv::Mat descriptor
  )
{ 
  // Find similarity of "descriptor" to the "db_subject-th" individual in the DB
  cv::Mat similarity = m_P * (cv::abs(new_descriptor - descriptor) - m_mean);
  
  return (double) similarity.at<float>(0,0);
}

// -----------------------------------------------------------------------------
//
// Purpose and Method:  
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
bool 
DCTComparator::isDistance
  ()
{
  return false;
}

// -----------------------------------------------------------------------------
//
// Purpose and Method:  
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
bool 
DCTComparator::isSimilarity
  ()
{
  return true;
}

}; }; // namespace