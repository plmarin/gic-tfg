// -----------------------------------------------------------------------------
/**
 *  @brief ...
 *  @author Jose M. Buenaposada, Pablo Marín
 *  @date 2013/5
 *  @version $revision$
 *
 *  $id$
 *
 *  Grupo de investigación en Percepción Computacional y Robótica)
 *  (Perception for Computers & Robots research Group)
 *  Facultad de Informática (Computer Science School)
 *  Universidad Politécnica de Madrid (UPM) (Madrid Technical University)
 *  http://www.dia.fi.upm.es/~pcr
 *
 */
// -----------------------------------------------------------------------------

// ------------------ RECURSION PROTECTION -------------------------------------
#ifndef DESCRIPTOR_COMPARATOR_HPP
#define DESCRIPTOR_COMPARATOR_HPP


// ----------------------- INCLUDES --------------------------------------------
#include <boost/shared_ptr.hpp>
#include <opencv/cv.h>
#include <opencv/ml.h>
#include <queue>

namespace upm { namespace pcr
{
  
class DescriptorComparator;
typedef boost::shared_ptr<DescriptorComparator> DescriptorComparatorPtr;

// ----------------------------------------------------------------------------
/**
 * @class DescriptorComparator
 * @brief ...
 */
// -----------------------------------------------------------------------------
  
class DescriptorComparator
{
public:
  
  DescriptorComparator
    () {};
  
  virtual 
  ~DescriptorComparator
    () {};
    
  /**
   * @brief Compare two descriptors
   *
   * @param new_descriptor
   * @param descriptor
   */
  virtual double
  compare
    (
    cv::Mat new_descriptor,
    cv::Mat descriptor
    ) = 0;  
  
  /**
   * @brief
   *
   * @param
   */
  virtual bool
  isDistance
    () = 0;
  
   /**
   * @brief
   *
   * @param
   */
  virtual bool
  isSimilarity
    () = 0;
  
};

}; }; // namespace


#endif // DESCRIPTOR_COMPARATOR_HPP
