// -----------------------------------------------------------------------------
/**
 *  @brief Implementation of face image region processor example.
 *  @author Jose M. Buenaposada
 *  @version $revision$
 *
 *  $id$
 *
 *  Grupo de investigación en Percepción Computacional y Robótica)
 *  (Perception for Computers & Robots research Group)
 *  Facultad de Informática (Computer Science School)
 *  Universidad Politécnica de Madrid (UPM) (Madrid Technical University)
 *  http://www.dia.fi.upm.es/~pcr
 *
 */
// -----------------------------------------------------------------------------
#include "face_processor_example.hpp"
#include <sstream>
#include "trace.hpp"

#define MIN_EYE_DIST_PERCENTAGE_OF_FACE_WIDTH 0.38f
#define MAX_EYE_LINE_ANGLE_RADIANS 0.17f // ~ 15 degrees


namespace upm { namespace pcr
{

// -----------------------------------------------------------------------------
//
// Purpose and Method:  
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
FaceProcessorExample::FaceProcessorExample
  (
  bool use_eyes, /* = true */
  bool do_temporal_integration /* = true */
  )
{
  m_use_eyes = use_eyes;
  
  m_pEqualizeMaskFilter.reset(new EqualizeMaskFilter(NULL, 25, 25, true));
  m_cropped_image_width  = 25;
  m_cropped_image_height = 25;
  m_pImage               = cvCreateImage(cvSize(m_cropped_image_width, 
                                                m_cropped_image_height), 
                                         IPL_DEPTH_8U, 1);
  m_pImageVector         = cvCreateMat(m_cropped_image_width*m_cropped_image_height, 
                                       1, CV_32FC1);
}

// -----------------------------------------------------------------------------
//
// Purpose and Method:  Copy constructor
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
FaceProcessorExample::FaceProcessorExample
  (
  const FaceProcessorExample& classifier
  )
{
  m_pImage          = NULL;
  m_pImageVector    = NULL;

  (*this)           = (classifier);
}

// -----------------------------------------------------------------------------
//
// Purpose and Method:  Asignment operator.
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
FaceProcessorExample&
FaceProcessorExample::operator =
  (
  const FaceProcessorExample& classifier
  )
{   
  if (&classifier == this)
  {
    return (*this);
  };

  m_cropped_image_width  = classifier.m_cropped_image_width;
  m_cropped_image_height = classifier.m_cropped_image_height;

  if (m_pImage != NULL)
    cvReleaseImage(&m_pImage);
  m_pImage               = cvCloneImage(classifier.m_pImage);
  
  if (m_pImageVector != NULL)
    cvReleaseMat(&m_pImageVector);
  m_pImageVector         = cvCloneMat(classifier.m_pImageVector);
 
  m_pEqualizeMaskFilter   = classifier.m_pEqualizeMaskFilter->clone();
    
  return (*this);  
}

// -----------------------------------------------------------------------------
//
// Purpose and Method:  
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
FaceProcessorExample::~FaceProcessorExample
  ()
{  
  if (m_pImage != NULL)
    cvReleaseImage(&m_pImage);
  
  if (m_pImageVector != NULL)
    cvReleaseMat(&m_pImageVector);
}

// -----------------------------------------------------------------------------
//
// Purpose and Method: 
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
FaceProcessorPtr
FaceProcessorExample::clone
  ()
{
  FaceProcessorPtr classifier_ptr(dynamic_cast<FaceProcessor*>(new FaceProcessorExample((*this))));

  return classifier_ptr;
}

// -----------------------------------------------------------------------------
//
// Purpose and Method: 
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
void
FaceProcessorExample::processFrame 
 (
 IplImage* pFrame,
 Face& f
 )
{
  ImageRegion region;
  int x, y, width, height;
  CvPoint left_eye, right_eye;
  double dist_eyes;
  double angle;

  f.getRegion(region);
  region.getParams(x, y, width, height);

  if (m_use_eyes)
  {
    f.getEyesRelativeCoordinates(left_eye, right_eye);

    // If we have not detected both eyes we do not process the frame
    if ( (left_eye.x  == -1) || (left_eye.y  == -1) ||
         (right_eye.x == -1) || (right_eye.y == -1))
    {
      return;
    }

    // Only if the face is almost frontal we sent it to be processed by 
    // our algorithm
    dist_eyes = sqrt(static_cast<double>(((right_eye.x - left_eye.x) *
                                          (right_eye.x - left_eye.x)) +
                                          ((right_eye.y - left_eye.y) *
                                          (right_eye.y - left_eye.y)) ));
    angle     = atan(static_cast<double>(right_eye.y - left_eye.y) /
                   static_cast<double>(right_eye.x - left_eye.x));
    angle     = (angle>0.0)?angle:-angle;

    if (angle > MAX_EYE_LINE_ANGLE_RADIANS)
    {
      // The face is rotated in plane 
      return;
    }
  } // if (use_eyes)

  // Crop, resize and normalise the face image region 
  (*m_pEqualizeMaskFilter)(pFrame, f, m_pImage);

  // Copy the equalized and masked image on pImageVector scanning the image
  // by columns
  for (int i=0; i < m_pImage->height; i++)
  {
    for(int j=0; j < m_pImage->width; j++)
    {
      double value = static_cast<double>(((uchar *)(m_pImage->imageData + 
                                                    i*m_pImage->widthStep))[j]);
      // The image vector scans the image by columns (like Matlab V(:)).
      cvmSet(m_pImageVector, (m_pImage->height*j)+i, 0, value);
    }
  }
}

// -----------------------------------------------------------------------------
//
// Purpose and Method: 
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
void
FaceProcessorExample::showResults
  (
  Viewer* pViewer,
  IplImage* pFrame,
  Face& f
  )
{
  int x_window, y_window;
  int window_width, window_height;
  float color[] = {1.0, 1.0, 1.0};
  std::ostringstream out;
  ImageRegion region;

  // Get the image region from the face.
  f.getRegion(region);

  region.getParams(x_window, y_window, window_width, window_height);

  // Plot cropped image.
  pViewer->image(m_pImage, x_window + window_width, 
                 y_window + 20, window_width*0.5, window_width*0.5);
};

// -----------------------------------------------------------------------------
//
// Purpose and Method: 
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
void
FaceProcessorExample::getAttributes
  (
  FaceAttributes& attributes
  )
{
  bool expression_attr_found = false;
  std::string attr_name      = "attribute";

  // Male probability
  FaceAttributePtr attribute_male_ptr(new FaceAttribute(attr_name, 
                                 FaceAttribute::ATTR_STRING_TYPE,
                                 "value1",
                                 0.9));
  attributes.insert(std::make_pair(attr_name, attribute_male_ptr));

  // Female probability
  FaceAttributePtr attribute_female_ptr(new FaceAttribute(attr_name, 
                                 FaceAttribute::ATTR_STRING_TYPE,
                                 "value2",
                                 0.9));
  attributes.insert(std::make_pair(attr_name, attribute_female_ptr));
};


}; }; // namespace

