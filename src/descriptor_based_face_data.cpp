
// -----------------------------------------------------------------------------
/**
 *  @brief Save and compare descriptors
 *  @author Jose M. Buenaposada, Pablo Marín
 *  @date 2013/5
 *  @version $revision$
 *
 *  $id$
 *
 *  Grupo de investigación en Percepción Computacional y Robótica)
 *  (Perception for Computers & Robots research Group)
 *  Facultad de Informática (Computer Science School)
 *  Universidad Politécnica de Madrid (UPM) (Madrid Technical University)
 *  http://www.dia.fi.upm.es/~pcr
 *
 */
// -----------------------------------------------------------------------------
#include "descriptor_based_face_data.hpp"
#include <limits>
#include <boost/concept_check.hpp>
#include "trace.hpp"

namespace upm { namespace pcr
{

static const float MINIMAL_NUMBER_OF_IMAGES = 20;
  
// -----------------------------------------------------------------------------
//
// Purpose and Method:  
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
DescriptorBasedFaceData::DescriptorBasedFaceData
  (
  DescriptorComparatorPtr descriptorComparator,
  ImageDescriptorPtr imageDescriptor
  ):
    m_descriptorComparator(descriptorComparator),
    m_imageDescriptor(imageDescriptor)
{
  this->m_images_added = 0;
}

// -----------------------------------------------------------------------------
//
// Purpose and Method:  
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
DescriptorBasedFaceData::DescriptorBasedFaceData
  (
  const FaceData* faceData
  )
{
  (*this) = (faceData);
}

// -----------------------------------------------------------------------------
//
// Purpose and Method:  
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
DescriptorBasedFaceData::~DescriptorBasedFaceData
  ()
{
}

// -----------------------------------------------------------------------------
//
// Purpose and Method:  
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
FaceDataPtr
DescriptorBasedFaceData::clone
  ()
{
  FaceDataPtr faceData_ptr(dynamic_cast<FaceData*>(new DescriptorBasedFaceData((*this))));

  return faceData_ptr;
}

// -----------------------------------------------------------------------------
//
// Purpose and Method: Compare two descriptors set
// Inputs:
// Outputs: similarity / distance is a matrix with the corresponding measures of 
//		each individual against new individual
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
cv::Mat
DescriptorBasedFaceData::compare
  (
  FaceDataPtr new_descriptors,		// New individual
  FaceDataPtr db_descriptors		// Data base individual
  )
{  
  cv::Mat measure;

  //DescriptorBasedFaceDataPtr new_ds(new DescriptorBasedFaceData(*static_cast<DescriptorBasedFaceData*>(new_descriptors.get())));
  //DescriptorBasedFaceDataPtr db_ds(new DescriptorBasedFaceData(*static_cast<DescriptorBasedFaceData*>(db_descriptors.get())));
  DescriptorBasedFaceDataPtr new_ds = boost::static_pointer_cast<DescriptorBasedFaceData>(new_descriptors);
  DescriptorBasedFaceDataPtr db_ds = boost::static_pointer_cast<DescriptorBasedFaceData>(db_descriptors);
  
  if (m_descriptorComparator->isSimilarity())
  {
    measure = this->obtainSimilarity(new_ds, db_ds);
  }
  else // m_descriptorComparator->isDistance() == true
  {
    measure = this->obtainDistance(new_ds, db_ds);
  }
  
  return measure;
}

// -----------------------------------------------------------------------------
//
// Purpose and Method: Compare two descriptors set
// Inputs:
// Outputs: similarity / distance is a matrix with the corresponding measures of 
//		each individual against new individual
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
cv::Mat
DescriptorBasedFaceData::obtainSimilarity
  (
  DescriptorBasedFaceDataPtr new_descriptors,		// New individual
  DescriptorBasedFaceDataPtr db_descriptors		// Data base individual
  )
{
  cv::Mat similarity(cv::Mat_<double>(1, new_descriptors->m_descriptors.size()));
  //cv::Mat similarity = cv::Mat(1, db_descriptors->m_descriptors.size(), cv::DataType<double>::type);
  
  for(int i = 0; i < new_descriptors->m_descriptors.size(); i++)
  {
    cv::Mat new_individual_descriptor = new_descriptors->m_descriptors.front();  // i-th image (new subject)
    new_descriptors->m_descriptors.pop();      
    new_descriptors->m_descriptors.push(new_individual_descriptor);

  // 	cv::Mat max_similarity_descriptor;
    double max_similarity = -(std::numeric_limits<double>::max());
  // 	int max_index         = -1;

    for(int d = 0; d < db_descriptors->m_descriptors.size(); d++)
    {
      cv::Mat descriptor = db_descriptors->m_descriptors.front();  // d-th descriptor of db_subject-th individual.
      db_descriptors->m_descriptors.pop();
      db_descriptors->m_descriptors.push(descriptor);
      
      double aux = this->m_descriptorComparator->compare(new_individual_descriptor, descriptor);
      
//std::cout << "SIMILARITY(" << d << ") --> " << aux << std::endl;
      
      if(aux > max_similarity) 
      {
        max_similarity = aux;
//        max_index      = d;      
//        max_similarity_descriptor = descriptor;
        similarity.row(0).col(i) = max_similarity;
      }
    }
//std::cout << "MAX_SIMILARITY(" << i << ") --> " << similarity.at<double>(i,0) << std::endl;
  }

  //      TRACE_INFO("similarity = " << (cv::Mat.ones(1, similarity.cols * similarity.rows) *
  //		  similarity.reshape(1, similarity.cols * similarity.rows)) << std::endl);
  return similarity;
}

cv::Mat 
DescriptorBasedFaceData::obtainDistance
  (
  DescriptorBasedFaceDataPtr new_descriptors, 	// New individual
  DescriptorBasedFaceDataPtr db_descriptors	// Data base individual
  )
{
  cv::Mat distance(cv::Mat_<double>(1, new_descriptors->m_descriptors.size()));

  for(int i = 0; i < new_descriptors->m_descriptors.size(); i++)
  {
    cv::Mat new_individual_descriptor = new_descriptors->m_descriptors.front();  // i-th image (new subject)
    new_descriptors->m_descriptors.pop();
    new_descriptors->m_descriptors.push(new_individual_descriptor);

//    cv::Mat min_distance_descriptor;
    double min_distance = std::numeric_limits<double>::max();
//    int min_index       = -1;  
    
    for(int d = 0; d < db_descriptors->m_descriptors.size(); d++)
    {
      cv::Mat descriptor = db_descriptors->m_descriptors.front();  // d-th descriptor of db_subject-th individual.
      db_descriptors->m_descriptors.pop();
      db_descriptors->m_descriptors.push(descriptor);
      
      double aux = this->m_descriptorComparator->compare(new_individual_descriptor, descriptor);
      
      if(aux < min_distance) 
      {
        min_distance = aux;
//        min_index      = d;      
//        min_distance_descriptor = descriptor;
        distance.row(0).col(i) = min_distance;
      }
    }
  }

  //      TRACE_INFO("distance = " << (cv::Mat.ones(1, distance.cols * distance.rows) * 
  //		 distance.reshape(1, distance.cols * distance.rows)) << std::endl);
  return distance;
}


// -----------------------------------------------------------------------------
//
// Purpose and Method:
// Inputs:
// Outputs: Number of images added
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
void
DescriptorBasedFaceData::addImage
  (
  cv::Mat image
  )
{
  cv::Mat descriptor = m_imageDescriptor->computeHighLevelDescriptor(image);
  m_descriptors.push(descriptor);
  
  if(m_descriptors.size() > MINIMAL_NUMBER_OF_IMAGES)
  {
    m_descriptors.pop();
  }
}

// -----------------------------------------------------------------------------
//
// Purpose and Method:  
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// ----------------------------------------------------------------------------- 
bool 
DescriptorBasedFaceData::isOk
  ()
{
  return ((this->m_descriptors.size() == MINIMAL_NUMBER_OF_IMAGES) ? true : false);
}
  

}; }; // namespace


