// -----------------------------------------------------------------------------
/**
 *  @brief ...
 *  @author Jose M. Buenaposada, Pablo Marín
 *  @date 2013/5
 *  @version $revision$
 *
 *  $id$
 *
 *  Grupo de investigación en Percepción Computacional y Robótica)
 *  (Perception for Computers & Robots research Group)
 *  Facultad de Informática (Computer Science School)
 *  Universidad Politécnica de Madrid (UPM) (Madrid Technical University)
 *  http://www.dia.fi.upm.es/~pcr
 *
 */
// -----------------------------------------------------------------------------

// ------------------ RECURSION PROTECTION -------------------------------------
#ifndef DESCRIPTOR_BASED_FACE_DATA_HPP
#define DESCRIPTOR_BASED_FACE_DATA_HPP

// ----------------------- INCLUDES --------------------------------------------
#include <boost/shared_ptr.hpp>
#include <opencv/cv.h>
#include <opencv/ml.h>
#include <limits>
#include "face_data.hpp"
#include "descriptor_comparator.hpp"
#include "image_descriptor.hpp"

namespace upm { namespace pcr
{

class DescriptorBasedFaceData;
typedef boost::shared_ptr<DescriptorBasedFaceData> DescriptorBasedFaceDataPtr;
  
// ----------------------------------------------------------------------------
/**
 * @class DescriptorBasedFaceData
 * @brief ...
 */
// -----------------------------------------------------------------------------

class DescriptorBasedFaceData : public FaceData
{
public:  
  
  typedef std::queue<cv::Mat> descriptorSet;
  descriptorSet m_descriptors;
  
  DescriptorBasedFaceData
    (
    DescriptorComparatorPtr descriptorComparator,
    ImageDescriptorPtr imageDescriptor
    );
    
  /** Copy constructor */  
  DescriptorBasedFaceData
    (
    const FaceData* faceData
    );

  virtual
  ~DescriptorBasedFaceData
    ();
    
  /**
   * @brief Returns a pointer to a copy of this object.
   */  
  virtual FaceDataPtr
  clone
    ();
  
  /**
   * @brief Compare two descriptors set
   *
   * @param new_descriptors
   * @param db_descriptors
   */
  virtual cv::Mat
  compare
    (
    FaceDataPtr new_descriptors,
    FaceDataPtr db_descriptors
    );
    
  /**
   * @brief Add an image to DescriptorBasedFaceData
   *
   * @param image
   */   
  virtual void
  addImage
    (
    cv::Mat image
    );  
  
  /**
   * @brief If there are enough images return true
   *
   * @param 
   */
  virtual bool
  isOk
    ();
    
    
private:
  
  DescriptorComparatorPtr m_descriptorComparator; 
  ImageDescriptorPtr m_imageDescriptor;
  long m_images_added;
  
  cv::Mat 
  obtainSimilarity
    (
    DescriptorBasedFaceDataPtr new_descriptors,
    DescriptorBasedFaceDataPtr db_descriptors
    );
    
  cv::Mat 
  obtainDistance
    (
    DescriptorBasedFaceDataPtr new_descriptors,
    DescriptorBasedFaceDataPtr db_descriptors
    );
      
};


}; }; // namespace

#endif // DESCRIPTOR_BASED_FACE_DATA_HPP

