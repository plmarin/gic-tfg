
#include <opencv/cv.h>
#include "viewer.hpp"
#include "faces_analyzer.hpp"
#include "biggest_face_tracker.hpp"
#include "similarity_warp_filter.hpp"
#include "face_processors_group.hpp"
#include "trace.hpp"
#include <iostream>
#include <iomanip> // setprecision
#include <sstream>
#include "create_haarcascade_ojoi.hpp"
#include "create_haarcascade_ojod.hpp"
#include <boost/program_options.hpp>

#define DETECTION_SCALE_STEP 1.2
#define DETECTION_MIN_NEIGHBORS 3
//#define	DETECTION_MIN_FACE_SIZE 80
#define DETECTION_MIN_FACE_SIZE 50 
#define CASCADE_FILE_LEFT_EYE  "./haarcascades/ojoI.xml"
#define CASCADE_FILE_RIGHT_EYE "./haarcascades/ojoD.xml"
#define DRAW_EYES true
#define USE_AGE_TEMP_INTEGRATION true
#define USE_EXPRESSIONS_TEMP_INTEGRATION true

/*
#define FACE_SIZE 255
#define SCALE (static_cast<double>(FACE_SIZE)/25.0)
//#define NORM_LEFT_EYE_X cvRound(5.0*SCALE)
#define NORM_LEFT_EYE_X cvRound(6.0*SCALE)
#define NORM_LEFT_EYE_Y cvRound(7.0*SCALE)
// #define NORM_RIGHT_EYE_X cvRound(19.0*SCALE)
#define NORM_RIGHT_EYE_X cvRound(18.0*SCALE)
#define NORM_RIGHT_EYE_Y cvRound(7.0*SCALE)
*/

#define TEMPLATE_FACE_SIZE 25.0
#define TEMPLATE_LEFT_EYE_X 6.0
#define TEMPLATE_LEFT_EYE_Y 7.0
#define TEMPLATE_RIGHT_EYE_X 18.0
#define TEMPLATE_RIGHT_EYE_Y 7.0

namespace po = boost::program_options;

// -----------------------------------------------------------------------------
//
// Purpose and Method:
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
int
cropInputFrame
  ( 
  IplImage* pFrame, 
  upm::pcr::Face& f,
  IplImage* pCroppedImage,
  CvPoint& norm_face_left_eye,
  CvPoint& norm_face_right_eye
  )
{
  int x, y;
  int width, height;
  IplImage* pGrayImage;
  upm::pcr::ImageRegion region;
  CvPoint left_eye, right_eye;
//  CvPoint norm_face_left_eye;
//  CvPoint norm_face_right_eye;
  double dist_src, dist_dst;
  double scale;
  double final_angle, init_angle;
    
  assert(pFrame != NULL);
  assert(pCroppedImage != NULL);
  
  // Crop face area from pFrame image and resize it to the size of the pFaceImage.
  // If we have detected both eyes we use its location to make a similarity
  // transform.
  f.getRegion(region);
  region.getParams(x, y, width, height);
  f.getEyesRelativeCoordinates(left_eye, right_eye);

  if ( (left_eye.x  == -1) || (left_eye.y  == -1) ||
       (right_eye.x == -1) || (right_eye.y == -1))
  {
    return -1;    
  }
    
/*
  norm_face_left_eye.x  = NORM_LEFT_EYE_X;
  norm_face_left_eye.y  = NORM_LEFT_EYE_Y;
  norm_face_right_eye.x = NORM_RIGHT_EYE_X;
  norm_face_right_eye.y = NORM_RIGHT_EYE_Y;
*/
  upm::pcr::SimilarityWarpFilter warpFilter(left_eye, right_eye, 
                                  norm_face_left_eye, norm_face_right_eye,
                                  cvSize(pCroppedImage->width, pCroppedImage->height));

  pGrayImage  = cvCreateImage(cvSize(pFrame->width, pFrame->height), 
			             pFrame->depth, 1);
  if (pFrame->nChannels == 3)
  {
    cvCvtColor( pFrame, pGrayImage, CV_RGB2GRAY );
  }
  else
  {
    cvCopy( pFrame, pGrayImage );
  }

  warpFilter( pGrayImage, f, pCroppedImage );
  
  cvReleaseImage( & pGrayImage );
  
  return 0;
};

int 
main
  ( 
  int argc, 
  char** argv 
  )
{
  float red_color[3]   = {1.0, 0.0, 0.0};
  CvHaarClassifierCascade* pCascade                 = NULL;
  CvHaarClassifierCascade* pCascadeLeftEye          = NULL;
  CvHaarClassifierCascade* pCascadeRightEye         = NULL;
  CvCapture* pCapture                               = NULL;
  IplImage *pFrame                                  = NULL;
  bool viewer_initialised                           = false;
  double t;
  std::vector<upm::pcr::FaceAttributes> faces_attributes;
  std::vector<upm::pcr::ImageRegion> faces_image_regions;
  int x, y, width, height;

  int optlen = strlen("--cascade=");
  std::string input_name;
  std::string cascade_name;
  int face_size = TEMPLATE_FACE_SIZE;
  float face_scale;
  CvPoint norm_face_left_eye;
  CvPoint norm_face_right_eye;
  
  TRACE_INFO(" Program started ..." << std::endl);
  
  try
  {
    
  // --------------------------------------------------------------
  // Declare the supported program options.
  po::options_description desc("Allowed options");
  desc.add_options()
    ("help", "produce help message")
    ("cascade", po::value<std::string>(), "set the face cascade file")
    ("cascade-left-eye", po::value<std::string>(), "set the left eye cascade file")
    ("cascade-right-eye", po::value<std::string>(), "set the right eye cascade file")
    ("face-size", po::value<int>(), "set the face cropped face size in pixels")
    ("input-name", po::value<std::string>(), "set the images source. Can be a video file, a camera index (from 0 to number of system cameras minus one) or a single picture file.")
  ;

  po::positional_options_description p;
  p.add("input-name", -1);

  po::variables_map vm;
//   po::store(po::parse_command_line(argc, argv, desc), vm);
  po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
  po::notify(vm);    

  if (vm.count("help")) {
      std::cout << desc << "\n";
      return 1;
  }
  
  if (vm.count("cascade"))
  {
    TRACE_INFO(" Cascade name supplied ..." << std::endl;)
    cascade_name = vm["cascade"].as<std::string>();
  }
  else
  {
    TRACE_INFO(" Default cascade name ..." << std::endl;)
    cascade_name = "./haarcascades/haarcascade_frontalface_alt2.xml";
  }

  TRACE_INFO(" Loading cascade:  "  << cascade_name << std::endl;)
  pCascade = (CvHaarClassifierCascade*)cvLoad( cascade_name.c_str(), 0, 0, 0 );
    
  if( !pCascade )
  {
    TRACE_ERROR("ERROR: Could not load classifier cascade" << std::endl;)
    std::cout << desc << "\n";
    return 0;
  }
  
  if (vm.count("cascade-left-eye"))
  {
    cascade_name = vm["cascade-left-eye"].as<std::string>();
  }
  else
  {
    cascade_name = CASCADE_FILE_LEFT_EYE;
  }
  
  pCascadeLeftEye  = (CvHaarClassifierCascade*)cvLoad( cascade_name.c_str() , 0, 0, 0 );
  if( !pCascadeLeftEye )
  {
    TRACE_ERROR("ERROR: Could not load left eye classifier cascade: ");
    TRACE_ERROR( cascade_name << std::endl;);
    return 0;
  }

  if (vm.count("cascade-right-eye"))
  {
    cascade_name = vm["cascade-right-eye"].as<std::string>();
  }
  else
  {
    cascade_name = CASCADE_FILE_RIGHT_EYE;
  }

  pCascadeRightEye = (CvHaarClassifierCascade*)cvLoad( cascade_name.c_str() , 0, 0, 0 );
  if( !pCascadeRightEye )
  {
    TRACE_ERROR("ERROR: Could not load right eye classifier cascade: ");
    TRACE_ERROR( CASCADE_FILE_RIGHT_EYE << std::endl;);
    return 0;
  }

/*
  // Create the cascade classifiers for the left and right eyes.
  pCascadeLeftEye  = createHaarcascade_ojoI();
  pCascadeRightEye = createHaarcascade_ojoD();
*/

  TRACE_INFO("Cascade created ..." << std::endl;)

  if (vm.count("input-name"))
  {
    input_name = vm["input-name"].as<std::string>();
  }

  if (vm.count("face-size"))
  {
    face_size        = vm["face-size"].as<int>();
    face_scale       = static_cast<double>(face_size)/TEMPLATE_FACE_SIZE;
    norm_face_left_eye.x  = cvRound(TEMPLATE_LEFT_EYE_X*face_scale);
    norm_face_left_eye.y  = cvRound(TEMPLATE_LEFT_EYE_Y*face_scale);
    norm_face_right_eye.x = cvRound(TEMPLATE_RIGHT_EYE_X*face_scale);
    norm_face_right_eye.y = cvRound(TEMPLATE_RIGHT_EYE_Y*face_scale);
  }
  else
  {
    face_size        = TEMPLATE_FACE_SIZE;
    norm_face_left_eye.x  = TEMPLATE_LEFT_EYE_X;
    norm_face_left_eye.y  = TEMPLATE_LEFT_EYE_Y;
    norm_face_right_eye.x = TEMPLATE_RIGHT_EYE_X;
    norm_face_right_eye.y = TEMPLATE_RIGHT_EYE_Y;
  }
  
  }
  catch(std::exception& e)  
  {
    std::cout << e.what() << "\n";
    return 1;
  }    
  
  if( input_name.empty() || (isdigit(input_name[0]) && input_name[1] == '\0') )
  {
    TRACE_INFO("Capturing from camera " << input_name << " ..." << std::endl;)
    pCapture = cvCaptureFromCAM( input_name.empty() ? 0 : input_name.c_str()[0]-'0' );
  }
  else
  {
    // Load the image from that filename
    pFrame = cvLoadImage( input_name.c_str(), 1 );

    if (!pFrame) 
    {
      TRACE_INFO("Capturing from AVI file " << input_name << " ..." << std::endl;)
      pCapture = cvCaptureFromAVI( input_name.c_str() ); 
    }
  }

  if ( !pCapture )
  {
    TRACE_ERROR("ERROR: Could not grab images" << std::endl;)
  }


  // Declare the viewer, the face detector/tracker and the filter to crop the faces.
  upm::pcr::Viewer viewer;
  upm::pcr::BiggestFaceTracker tracker(pCascade, pCascadeLeftEye, pCascadeRightEye, 
                          DETECTION_SCALE_STEP, DETECTION_MIN_NEIGHBORS, DETECTION_MIN_FACE_SIZE);
  IplImage* pCroppedImage = cvCreateImage(cvSize(face_size, face_size), IPL_DEPTH_8U, 1);
  int frame_num = 0;

  if (pCapture)
  {
    for(;;)
    {
      if ( !cvGrabFrame( pCapture ))
      {
        break;
      }

      pFrame = cvRetrieveFrame( pCapture );

      if ( !pFrame )
      {
        break;
      }

      if (!viewer_initialised)
      {
        viewer.init(pFrame->width, pFrame->height, "Crop images");
        viewer_initialised = true;
      }

      // Process frame cropping all the detected faces from the input image.
      upm::pcr::Faces tracked_faces;
      t = static_cast<double>(cvGetTickCount());
      tracker.processFrame(pFrame, tracked_faces);

      upm::pcr::Faces::iterator it = tracked_faces.begin();
      int num_face = 0;
      for(; it != tracked_faces.end(); it++)
      {
        if (cropInputFrame(pFrame, *(*it), pCroppedImage, norm_face_left_eye, norm_face_right_eye) == 0)
	{
	  std::ostringstream outs;  // Declare an output string stream.
          outs << std::setprecision(6) << frame_num << "_" << num_face << ".png" << std::ends;
          std::string file_name = outs.str();  
	  cvSaveImage(file_name.c_str(), pCroppedImage);
	}
	num_face++;
      }
      t = static_cast<double>(cvGetTickCount()) - t;
      std::ostringstream outs;  // Declare an output string stream.
      outs << "FPS=" << std::setprecision(3) << (static_cast<double>(cvGetTickFrequency())*1000.*1000.)/t << std::ends;
      std::string fps_info = outs.str();      // Get the created string from the output stream.

      // Drawing results
      viewer.beginDrawing();
      viewer.image(pFrame, 0, 0, pFrame->width, pFrame->height);
      tracker.showResults(&viewer, pFrame, tracked_faces);
      viewer.text(fps_info, 20, pFrame->height-20, red_color, 0.5);
      viewer.endDrawing();

      frame_num++;
    }
    cvReleaseCapture( &pCapture );
    cvReleaseImage( &pCroppedImage );
  }
  // If the capture is not loaded succesfully, then:
  else
  {
    TRACE_INFO("Loading image " << input_name << "from file  ..." << std::endl;)

    if (!pFrame)
    {
      TRACE_ERROR("ERROR: Could not find image file " << input_name << std::endl;)
    }

    viewer.init(pFrame->width, pFrame->height, "Crop images");

    // Process frame cropping all the detected faces from the input image.
    upm::pcr::Faces tracked_faces;
    t = static_cast<double>(cvGetTickCount());
    tracker.processFrame(pFrame, tracked_faces);

    upm::pcr::Faces::iterator it = tracked_faces.begin();
    int num_face = 0;
    for(; it != tracked_faces.end(); it++)
    {
      if (cropInputFrame(pFrame, *(*it), pCroppedImage, norm_face_left_eye, norm_face_right_eye) == 0)
      {
	  std::ostringstream outs;  // Declare an output string stream.
          outs << std::setprecision(6) << frame_num << "_" << num_face << ".png" << std::ends;
          std::string file_name = outs.str();  
	  cvSaveImage(file_name.c_str(), pCroppedImage);
      }
	num_face++;
    }
    t = static_cast<double>(cvGetTickCount()) - t;

    // Drawing results
    viewer.beginDrawing();
    viewer.image(pFrame, 0, 0, pFrame->width, pFrame->height);
    tracker.showResults(&viewer, pFrame, tracked_faces);

    // Release the image memory
    cvReleaseImage( &pFrame );
  }

return 0;
}

