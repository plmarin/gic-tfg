// -----------------------------------------------------------------------------
/**
 *  @brief Implementation of Face processors group.
 *  @author Jose M. Buenaposada
 *  @date 2011/01/29
 *  @version $revision$
 *
 *  $id$
 *
 *  Grupo de investigación en Percepción Computacional y Robótica)
 *  (Perception for Computers & Robots research Group)
 *  Facultad de Informática (Computer Science School)
 *  Universidad Politécnica de Madrid (UPM) (Madrid Technical University)
 *  http://www.dia.fi.upm.es/~pcr
 *
 */
// -----------------------------------------------------------------------------

#include "face.hpp"
#include "face_processors_group.hpp"
#include "trace.hpp"

namespace upm { namespace pcr
{

FaceProcessorsGroup::FaceProcessorsGroup
  ()
{
}
 
// -----------------------------------------------------------------------------
//
// Purpose and Method:  Copy constructor
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
FaceProcessorsGroup::FaceProcessorsGroup
  (
  const FaceProcessorsGroup& group
  )
{
  if (&group == this)
  {
    return;
  };
  
  (*this)=group;
}

// -----------------------------------------------------------------------------
//
// Purpose and Method:  Asignement operator.
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
FaceProcessorsGroup&
FaceProcessorsGroup::operator =
  (
  const FaceProcessorsGroup& group
  )
{
  // We empty this object image region processors list.
  this->m_processors.clear();

// Copy the list of image region processors from "group" into this object.
  FaceProcessors::const_iterator it = group.m_processors.begin();
  for(; it != group.m_processors.end(); it++)
  {
     this->m_processors.push_back((*it)->clone());
  };

  return (*this);
}

// -----------------------------------------------------------------------------
//
// Purpose and Method: 
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
FaceProcessorPtr
FaceProcessorsGroup::clone
  ()
{
  FaceProcessorPtr group_ptr(dynamic_cast<FaceProcessor*>(new FaceProcessorsGroup((*this))));

  return group_ptr;
}

// -----------------------------------------------------------------------------
//
// Purpose and Method: 
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
void
FaceProcessorsGroup::addProcessor
  (
  FaceProcessorPtr processor_ptr
  ) 
{
  m_processors.push_back(processor_ptr); 
};


// -----------------------------------------------------------------------------
//
// Purpose and Method: 
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
void
FaceProcessorsGroup::processFrame 
 (
 IplImage* pFrame,
 Face& f
 )
{
  FaceProcessors::iterator it;

  it = m_processors.begin();
  for(; it != m_processors.end(); it++)
  {
    (*it)->processFrame(pFrame, f);
  };
}

// -----------------------------------------------------------------------------
//
// Purpose and Method: 
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
void
FaceProcessorsGroup::showResults
  (
  Viewer* pViewer,
  IplImage* pFrame,
  Face& f
  )
{
  FaceProcessors::iterator it;

  it = m_processors.begin();
  for(; it != m_processors.end(); it++)
  {
    (*it)->showResults(pViewer, pFrame, f);
  };
}

// -----------------------------------------------------------------------------
//
// Purpose and Method: 
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
void
FaceProcessorsGroup::getAttributes
  (
  FaceAttributes& attributes
  )
{
  FaceProcessors::iterator it;

  it = m_processors.begin();
  for(; it != m_processors.end(); it++)
  {
    (*it)->getAttributes(attributes);
  };
}

}; }; // namespace

