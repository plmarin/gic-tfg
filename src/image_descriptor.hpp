// -----------------------------------------------------------------------------
/**
 *  @brief ...
 *  @author Jose M. Buenaposada, Pablo Marín
 *  @date 2013/5
 *  @version $revision$
 *
 *  $id$
 *
 *  Grupo de investigación en Percepción Computacional y Robótica)
 *  (Perception for Computers & Robots research Group)
 *  Facultad de Informática (Computer Science School)
 *  Universidad Politécnica de Madrid (UPM) (Madrid Technical University)
 *  http://www.dia.fi.upm.es/~pcr
 *
 */
// -----------------------------------------------------------------------------

// ------------------ RECURSION PROTECTION -------------------------------------
#ifndef IMAGE_DESCRIPTOR_HPP
#define IMAGE_DESCRIPTOR_HPP

// ----------------------- INCLUDES --------------------------------------------
#include <boost/shared_ptr.hpp>
#include <opencv/cv.h>

namespace upm { namespace pcr
{

class ImageDescriptor;
typedef boost::shared_ptr<ImageDescriptor> ImageDescriptorPtr;
  
// ----------------------------------------------------------------------------
/**
 * @class ImageDescriptor
 * @brief ...
 */
// -----------------------------------------------------------------------------

class ImageDescriptor
{
public:

  ImageDescriptor
    () {};
  
  virtual 
  ~ImageDescriptor
    () {};
    
  /**
   * @brief Compute the subjects distances based high level descriptor
   */
  virtual cv::Mat
  computeHighLevelDescriptor
    (
    cv::Mat low_level_descriptor
    ) = 0;
  
};

}; }; // namespace


#endif // IMAGE_DESCRIPTOR_HPP
