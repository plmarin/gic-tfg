// -----------------------------------------------------------------------------
/**
 *  @brief FaceTracker interface definition.
 *  @author Jose M. Buenaposada
 *  @date 2009/13/10
 *  @version $revision$
 *
 *  $id$
 *
 *  Grupo de investigación en Percepción Computacional y Robótica)
 *  (Perception for Computers & Robots research Group)
 *  Facultad de Informática (Computer Science School)
 *  Universidad Politécnica de Madrid (UPM) (Madrid Technical University)
 *  http://www.dia.fi.upm.es/~pcr
 *
 */
// -----------------------------------------------------------------------------

// ------------------ RECURSION PROTECTION -------------------------------------
#ifndef FACES_TRACKER_HPP
#define FACES_TRACKER_HPP

// ----------------------- INCLUDES --------------------------------------------
#include "viewer.hpp"
#include "face.hpp"
#include <boost/shared_ptr.hpp>

namespace upm { namespace pcr
{

// ----------------------------------------------------------------------------
/**
 *  Definition of an smart pointer to FacesTracker object 
 */
// -----------------------------------------------------------------------------
class FacesTracker;
typedef boost::shared_ptr<FacesTracker> FacesTrackerPtr;

// ----------------------------------------------------------------------------
/**
 * @class FacesTracker
 * @brief An interface for detecing and tracking all faces on a video sequence
 */
// -----------------------------------------------------------------------------
class FacesTracker
{
public:
  
  FacesTracker
    () {};

  virtual 
  ~FacesTracker
    () {};

  /**
   * @brief Detect and track faces on the input image.
   *
   * @param pImage an IplImage OpenCv image.
   * @param tracked_faces Container with the faces found.
   */
  virtual void 
  processFrame 
    (
    IplImage *pImage,
    Faces& tracked_faces
    ) = 0;

  /**
   * @brief Visualise results after processFrame call.
   *
   * @param pViewer The "Graphic Window" over to which draw the results.
   * @param pImage The last processed frame by a "processFrame" call.
   * @param tracked_faces Container with the faces found in the last "processFrame" call.
   */
  virtual void 
  showResults
    (
    Viewer* pViewer,
    IplImage *pImage,
    Faces& tracked_faces
    ) = 0;

  /**
   * @brief Gets the list of detected faces
   */
  /*
  void
  getTrackedFaces
    (
    Faces& tracked_faces
    );
    */
};

}; }; // namespace

#endif
