// -----------------------------------------------------------------------------
/**
 *  @brief ...
 *  @author Jose M. Buenaposada, Pablo Marín
 *  @date 2013/5
 *  @version $revision$
 *
 *  $id$
 *
 *  Grupo de investigación en Percepción Computacional y Robótica)
 *  (Perception for Computers & Robots research Group)
 *  Facultad de Informática (Computer Science School)
 *  Universidad Politécnica de Madrid (UPM) (Madrid Technical University)
 *  http://www.dia.fi.upm.es/~pcr
 *
 */
// -----------------------------------------------------------------------------

// ------------------ RECURSION PROTECTION -------------------------------------
#ifndef DCT_COMPARATOR_HPP
#define DCT_COMPARATOR_HPP

// ----------------------- INCLUDES --------------------------------------------
#include "descriptor_comparator.hpp"


namespace upm { namespace pcr
{
  
// ----------------------------------------------------------------------------
/**
 * @class DCTComparator
 * @brief ...
 */
// -----------------------------------------------------------------------------

class DCTComparator : public DescriptorComparator
{
public:
  
  DCTComparator
    ();
  
  virtual
  ~DCTComparator
    ();
  
  /**
   * @brief Compare two descriptors
   *
   * @param new_descriptor
   * @param descriptor
   */
  virtual double
  compare
    (
    cv::Mat new_descriptor,
    cv::Mat descriptor
    );  
  
  /**
   * @brief
   *
   * @param
   */
  virtual bool
  isDistance
    ();
  
   /**
   * @brief
   *
   * @param
   */
  virtual bool
  isSimilarity
    ();
    
private:
  
  cv::Mat m_P;
  cv::Mat m_mean;
  
};

}; }; // namespace


#endif // DCT_COMPARATOR_HPP
