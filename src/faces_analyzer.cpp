// -----------------------------------------------------------------------------
/**
 *  @brief FacesAnalyzer implementation
 *  @author Jose M. Buenaposada
 *  @date 2009/13/10
 *  @version $revision$
 *
 *  $id$
 *
 *  Grupo de investigación en Percepción Computacional y Robótica)
 *  (Perception for Computers & Robots research Group)
 *  Facultad de Informática (Computer Science School)
 *  Universidad Politécnica de Madrid (UPM) (Madrid Technical University)
 *  http://www.dia.fi.upm.es/~pcr
 *
 */
// -----------------------------------------------------------------------------

// ----------------------- INCLUDES --------------------------------------------
#include "faces_analyzer.hpp"
#include <algorithm> // std::copy
#include "trace.hpp"

namespace upm { namespace pcr
{

// -----------------------------------------------------------------------------
//
// Purpose and Method: Constructor
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
FacesAnalyzer::FacesAnalyzer
  (
  FacesTrackerPtr faces_tracker_ptr,
  FaceProcessorPtr processor_prototype_ptr
  ): 
  m_faces_tracker_ptr(faces_tracker_ptr),
  m_processor_prototype_ptr(processor_prototype_ptr)
{
};

// -----------------------------------------------------------------------------
//
// Purpose and Method: Destructor
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
FacesAnalyzer::~FacesAnalyzer
  ()
{
};

// -----------------------------------------------------------------------------
//
// Purpose and Method: 
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
void 
FacesAnalyzer::processFrame 
  (
  IplImage *pImage
  )
{
  Faces::iterator it;

  m_faces_tracker_ptr->processFrame(pImage, m_tracked_faces);   

  // Set a new copy of the image processor to each new face (hasProcessor() 
  // will be false for new faces) and apply processFaceRegion to each of 
  // the faces.
  it = m_tracked_faces.begin();
  for(; it != m_tracked_faces.end(); it++)
  {
    if (!(*it)->hasProcessor())
    {
      (*it)->setProcessor(m_processor_prototype_ptr->clone());  
    }

    (*it)->processFaceRegion(pImage);
  }
};

// -----------------------------------------------------------------------------
//
// Purpose and Method: 
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
void 
FacesAnalyzer::showResults
  (
  Viewer* pViewer,
  IplImage *pImage
  )
{
  Faces::iterator it;

  m_faces_tracker_ptr->showResults(pViewer, pImage, m_tracked_faces);
   
  // Show results of the face region processors 
  it = m_tracked_faces.begin();
  for(; it != m_tracked_faces.end(); it++)
  {
    (*it)->showResults(pViewer, pImage);
  }
};

// -----------------------------------------------------------------------------
//
// Purpose and Method: 
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
void
FacesAnalyzer::getTrackedFacesInfo
  (
  std::vector<FaceAttributes>& faces_attributes,
  std::vector<ImageRegion>& faces_image_regions
  )
{
  faces_attributes.clear();
  faces_attributes.resize(m_tracked_faces.size());

  faces_image_regions.clear();
  faces_image_regions.resize(m_tracked_faces.size());

  for (int i=0; i<m_tracked_faces.size(); i++)
  {
    m_tracked_faces[i]->getAttributes(faces_attributes[i]);
    m_tracked_faces[i]->getRegion(faces_image_regions[i]);
  }
};

}; }; // namespace
