// -----------------------------------------------------------------------------
/**
 *  @brief Face example image processor interface definition
 *  @author Jose M. Buenaposada
 *  @version $revision$
 *
 *  $id$
 *
 *  Grupo de investigación en Percepción Computacional y Robótica)
 *  (Perception for Computers & Robots research Group)
 *  Facultad de Informática (Computer Science School)
 *  Universidad Politécnica de Madrid (UPM) (Madrid Technical University)
 *  http://www.dia.fi.upm.es/~pcr
 *
 */
// -----------------------------------------------------------------------------

// ------------------ RECURSION PROTECTION -------------------------------------
#ifndef GENDER_CLASSIFIER_HPP
#define GENDER_CLASSIFIER_HPP

// ----------------------- INCLUDES --------------------------------------------
//#include "knn_classifier.hpp"
#include "face_processor.hpp"
#include "equalize_mask_filter.hpp"
#include <opencv/cv.h>
#include <opencv/ml.h>

namespace upm { namespace pcr
{

// ----------------------------------------------------------------------------
/**
 * @class FaceProcessorExample
 * @brief An image region processor subclass for face expressions classification
 */
// -----------------------------------------------------------------------------
class FaceProcessorExample: public FaceProcessor
{
public:

  FaceProcessorExample
    (
    bool use_eyes = true, 
    bool do_temporal_integration = true
    );

  /** Copy constructor */
  FaceProcessorExample
    (
    const FaceProcessorExample& classifier
    );

  /** Asignment operator */
  FaceProcessorExample&
  operator =
    (
    const FaceProcessorExample& classifier
    );

  ~FaceProcessorExample
    ();

  /**
   * @brief Do something on the face image region.
   *
   * @param pFrame Image comming from the camera.
   * @param f The face within pFrame to process.
   */
  void
  processFrame 
   (
   IplImage* pFrame,
   Face& f 
   );

  /**
   * @brief Shows the regression results.
   *
   * @param pViewer The viewer over to which display results.
   * @param pFrame Last processed camera frame.
   * @param f The face within pFrame that was used in the last call to processFrame.
   */
  void
  showResults
   (
   Viewer* pViewer,
   IplImage* pFrame,
   Face& f
   );

  /**
   * @brief Returns a pointer to a copy of this object itself.
   */
  FaceProcessorPtr
  clone
   ();

  /**
   * @brief Get the face attributes computed by the image region processor.
   *
   * @param attributes
   *
   * An example of the kind of attributes that this region processor generates
   * is:
   *     "atributte" ATTR_STRING_TYPE "value1" 0.95
   *     "atributte" ATTR_STRING_TYPE "value2" 0.05
   *
   * In this case the confidence is equal to the probability of the gender.
   *
   */
  void
  getAttributes
    (
    FaceAttributes& attributes
    );

private:
  IplImage*    m_pImage;
  CvMat*       m_pImageVector;
  unsigned int m_cropped_image_width; 
  unsigned int m_cropped_image_height; 
  EqualizeMaskFilterPtr m_pEqualizeMaskFilter;
  bool m_use_eyes;
};

}; }; // namespace

#endif

