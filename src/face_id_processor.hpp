// -----------------------------------------------------------------------------
/**
 *  @brief Face recognition.
 *  @author Jose M. Buenaposada, Pablo Marín
 *  @date 2012/4
 *  @version $revision$
 *
 *  $id$
 *
 *  Grupo de investigación en Percepción Computacional y Robótica)
 *  (Perception for Computers & Robots research Group)
 *  Facultad de Informática (Computer Science School)
 *  Universidad Politécnica de Madrid (UPM) (Madrid Technical University)
 *  http://www.dia.fi.upm.es/~pcr
 *
 */
// -----------------------------------------------------------------------------

// ------------------ RECURSION PROTECTION -------------------------------------
#ifndef FACE_ID_PROCESSOR_HPP
#define FACE_ID_PROCESSOR_HPP

// ----------------------- INCLUDES --------------------------------------------
#include "face_processor.hpp"
#include "face_id_database.hpp"
#include "face_data.hpp"
#include <opencv/cv.h>
#include <opencv/ml.h>
#include <set>

namespace upm { namespace pcr
{

// ----------------------------------------------------------------------------
/**
 * @class FaceIdProcessor
 * @brief An image region processor subclass for face recognition
 * 
 */

// -----------------------------------------------------------------------------
class FaceIdProcessor: public FaceProcessor
{
public:

  FaceIdProcessor
    (
    FaceDataPtr face_data,
    FaceIdDataBasePtr face_id_db,
    bool use_eyes = false
    );

  /** Copy constructor */
  FaceIdProcessor
    (
    const FaceIdProcessor& processor
    );

  /** Asignment operator */
  FaceIdProcessor&
  operator =
    (
    const   FaceIdProcessor& processor
    );

  ~FaceIdProcessor
    ();

  /**
   * @brief Process face region for recognition
   *
   * @param pFrame Image comming from the camera.
   * @param f The face within pFrame to process.
   */
  void
  processFrame
   (
   IplImage* pFrame,
   Face& f 
   );

  /**
   * @brief Shows the recognition results (Face ID)
   *
   * @param pViewer The viewer over to which display results.
   * @param pFrame Last processed camera frame.
   * @param f The face within pFrame that was used in the last call to processFrame.
   */
  void
  showResults
   (
   Viewer* pViewer,
   IplImage* pFrame,
   Face& f
   );

  /**
   * @brief Returns a pointer to a copy of this object.
   */
  FaceProcessorPtr
  clone
   ();

  /**
   * @brief Get the face attributes computed by the face region processor.
   *
   * @param attributes
   *
   * An example of the kind of attributes that this region processor generates
   * is:
   *     "face_id" ATTR_STRING_TYPE "value1" 0.95
   *
   * In this case the confidence is equal to the probability of the identification.
   *
   */
  void
  getAttributes
    (
    FaceAttributes& attributes
    );

private:
  cv::Mat m_image;
//  cv::Mat m_P;
//  cv::Mat m_mean;
  
  /** The face images descriptors seen so far **/
  //std::queue<cv::Mat> m_face_descriptors_seen;  
  
  float m_face_id_confidence;
  FaceIdDataBase::id_type m_face_id;
  FaceIdDataBasePtr m_face_id_db;
  FaceDataPtr m_face_data; 

  CvPoint m_norm_left_eye;
  CvPoint m_norm_right_eye;
  bool m_use_eyes;
  
  std::vector<cv::Mat> m_first_images;
  std::vector<cv::Mat> m_last_images;
  
  /**
   * @brief Save images into vector
   */
  void
  saveImages
    ();
    
  /**
   * @brief Write image
   */
  void
  saveImage
    ();  
  
};

} } // namespace

#endif

