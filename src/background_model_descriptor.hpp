// -----------------------------------------------------------------------------
/**
 *  @brief Face Background Model to compute Distances to faces in a Fixed DB.
 *  @author Jose M. Buenaposada, Pablo Marín
 *  @date 2013/5
 *  @version $revision$
 *
 *  $id$
 *
 *  Grupo de investigación en Percepción Computacional y Robótica)
 *  (Perception for Computers & Robots research Group)
 *  Facultad de Informática (Computer Science School)
 *  Universidad Politécnica de Madrid (UPM) (Madrid Technical University)
 *  http://www.dia.fi.upm.es/~pcr
 *
 */
// -----------------------------------------------------------------------------

// ------------------ RECURSION PROTECTION -------------------------------------
#ifndef BACKGROUND_MODEL_DESCRIPTOR_HPP
#define BACKGROUND_MODEL_DESCRIPTOR_HPP

// ----------------------- INCLUDES --------------------------------------------
#include <opencv/cv.h>
#include <opencv/ml.h>
#include "distance.hpp"
#include <boost/shared_ptr.hpp>
#include "lda_model_subspace.hpp"
#include "image_descriptor.hpp"

namespace upm { namespace pcr
{

class BackgroundModelDescriptor;
typedef boost::shared_ptr<BackgroundModelDescriptor> BackgroundModelDescriptorPtr;

typedef boost::shared_ptr<cv::flann::Index> KdTreePtr;  
  
// ----------------------------------------------------------------------------
/**
 * @class BackgroundModelDescriptor
 * @brief Computes how similar is an input face to a set of predefined faces (background model)
 * 
 * In this class we have stored a set of low level descriptors of the background 
 * model faces (for example the images of 68 subjects in PIE Database). We 
 * distance of a given image low level descriptor with all the individuals in the
 * Background model and we get the high level descriptor for the input face. 
 */
// -----------------------------------------------------------------------------
class BackgroundModelDescriptor : public ImageDescriptor
{
public:

  BackgroundModelDescriptor
    ();
    
  BackgroundModelDescriptor
    (
    LDAModelSubspacePtr lda_model_subspace
    );
    
  virtual 
  ~BackgroundModelDescriptor
    ();

  /**
   * @brief Compute the subjects distances based high level descriptor
   */
  virtual cv::Mat
  computeHighLevelDescriptor
    (
    cv::Mat low_level_descriptor
    );
    
  std::vector<int>
  computeTau
    (
    std::vector<int>& indices
    );
  
    
private:
  
  std::vector<KdTreePtr> m_subject_kdtrees;
  LDAModelSubspacePtr m_lda_model_subspace;
  
};

}; }; // namespace

#endif // BACKGROUND_MODEL_DESCRIPTOR_HPP
