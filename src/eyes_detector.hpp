// -----------------------------------------------------------------------------
/**
 *  @brief EyesDetector interface definition.
 *  @author Jose M. Buenaposada
 *  @date 2011/01/26
 *  @version $revision$
 *
 *  $id$
 *
 *  Grupo de investigación en Percepción Computacional y Robótica)
 *  (Perception for Computers & Robots research Group)
 *  Facultad de Informática (Computer Science School)
 *  Universidad Politécnica de Madrid (UPM) (Madrid Technical University)
 *  http://www.dia.fi.upm.es/~pcr
 *
 */
// -----------------------------------------------------------------------------

// ------------------ RECURSION PROTECTION -------------------------------------
#ifndef EYES_DETECTOR_HPP
#define EYES_DETECTOR_HPP

// ----------------------- INCLUDES --------------------------------------------
/*#include <boost/shared_ptr.hpp>
#include <vector>*/
#include "image_region.hpp"
#include "viewer.hpp"
#include <opencv/cv.h>

namespace upm { namespace pcr
{

// -----------------------------------------------------------------------------
/**
 * @class EyesDetector
 * @brief A class that stores information about the state of a known face
 */
// -----------------------------------------------------------------------------
class EyesDetector
{
public:

  EyesDetector
    (
    CvHaarClassifierCascade* pCascadeLeft,
    CvHaarClassifierCascade* pCascadeRight,
    int& orig_face_window_size
    );

  ~EyesDetector
    ();

  /**
   * @brief Computes the relative position of the eyes with respect to the face region.
   *
   * Bear in mind that here left and right are image concepts. The left_eye_pos are the coordinates 
   * of the eye appearing in the image pn the left.
   *
   * @param pFrame Image in which the face region is located.
   * @param region Squared area of the image in which the face is located.
   * @param left_eye Pixel coordinates relative to the top left corner of the face image region.
   * @param right_eye Pixel coordinates relative to the top left corner of the face image region.
   */
  void
  detect
    (
    IplImage* pFrame,
    upm::pcr::ImageRegion& region, 
    CvPoint& left_eye,
    CvPoint& right_eye
    );

  /**
   * @brief Shows the eye detection results
   *
   * Bear in mind that here left and right are image concepts. The left_eye_pos are the coordinates 
   * of the eye appearing in the image pn the left.
   *
   * @param pViewer Canvas to draw on. 
   * @param pFrame Image in which the face region is located.
   * @param region Squared area of the image in which the face is located.
   * @param left_eye Pixel coordinates relative to the top left corner of the face image region.
   * @param right_eye Pixel coordinates relative to the top left corner of the face image region.
   */
  void 
  showResults
    (
    upm::pcr::Viewer* pViewer,
    IplImage *pFrame,
    upm::pcr::ImageRegion region, 
    CvPoint left_eye,
    CvPoint right_eye
    );

private:
  
  void 
  detectSingleEye
    (
    IplImage* pFrame,
    ImageRegion& region, 
    CvPoint& eye_pos,
    CvPoint& offset,
    CvHaarClassifierCascade* pCascade,
    double face_scale
    );
  
  CvMemStorage* m_pStorage;
  CvHaarClassifierCascade* m_pCascadeLeft;
  CvHaarClassifierCascade* m_pCascadeRight;
  
  // This is equal to the original window size of left eye plus
  // the original window size of the right eye.
  double m_orig_face_window_size;
};

}; }; // namespace

#endif
