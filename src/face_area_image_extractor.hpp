// -----------------------------------------------------------------------------
/**
 *  @brief Face area image extractor interface definition
 *  @author Jose M. Buenaposada
 *  @date 2011/06/09
 *  @version $revision$
 *
 *  $id$
 *
 *  Grupo de investigación en Percepción Computacional y Robótica)
 *  (Perception for Computers & Robots research Group)
 *  Facultad de Informática (Computer Science School)
 *  Universidad Politécnica de Madrid (UPM) (Madrid Technical University)
 *  http://www.dia.fi.upm.es/~pcr
 *
 */
// -----------------------------------------------------------------------------

// ------------------ RECURSION PROTECTION -------------------------------------
#ifndef FACE_AREA_IMAGE_EXTRACTOR_HPP
#define FACE_AREA_IMAGE_EXTRACTOR_HPP

// ----------------------- INCLUDES --------------------------------------------
#include <boost/shared_ptr.hpp>
#include "face.hpp"
#include <vector>
#include <opencv/cv.h>

namespace upm { namespace pcr
{
// ----------------------------------------------------------------------------
/**
 *  Definition of an smart pointer to FaceAreaImageExtractor type
 */
// -----------------------------------------------------------------------------
class FaceAreaImageExtractor;
typedef boost::shared_ptr<FaceAreaImageExtractor> FaceAreaImageExtractorPtr;

// -----------------------------------------------------------------------------
/**
 *  Definition of a container of smart pointers to FaceAreaImageExtractor
 */
// -----------------------------------------------------------------------------
typedef std::vector<FaceAreaImageExtractorPtr> FaceAreaImageExtractors;

// ----------------------------------------------------------------------------
/**
 * @class FaceAreaImageExtractor
 * @brief Defines the interface for filtering the face area image.
 */
// -----------------------------------------------------------------------------
class FaceAreaImageExtractor
{
public:

  FaceAreaImageExtractor
    () {};

  virtual ~FaceAreaImageExtractor
    () {};

  /**
   * @brief Process an interesting image region 
   *
   * @param  pFrame Image comming from the camera.
   * @param  f The face on pFrame to process.
   * @return pExtractedFeatures. 
   */
  virtual void
  operator ()
   (
   IplImage* pFrame,
   Face& f,
   CvMat* pExtractedFeatures
   ) = 0;
};

}; }; // namespace

#endif
