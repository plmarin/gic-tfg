// -----------------------------------------------------------------------------
/**
 *  @brief Distance between permutations vectors implementation
 *  @author Jose M. Buenaposada, Pablo Marín
 *  @date 2012/4
 *  @version $revision$
 *
 *  $id$
 *
 *  Grupo de investigación en Percepción Computacional y Robótica)
 *  (Perception for Computers & Robots research Group)
 *  Facultad de Informática (Computer Science School)
 *  Universidad Politécnica de Madrid (UPM) (Madrid Technical University)
 *  http://www.dia.fi.upm.es/~pcr
 *
 */
// -----------------------------------------------------------------------------
#include "permutations_distance.hpp"

namespace upm { namespace pcr
{

PermutationsDistance::PermutationsDistance
  () : Distance()
{
}

PermutationsDistance::~PermutationsDistance
  ()
{
}

double
PermutationsDistance::computeDistance
  (
  CvMat* d1,
  CvMat* d2
  )
{
  return computeDistance(cv::Mat(d1,true), cv::Mat(d2,true));
}

double
PermutationsDistance::computeDistance
  (
  cv::Mat d1,
  cv::Mat d2
  )
{
  return 0.0;
}

}; }; // namespace


