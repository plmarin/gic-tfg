// -----------------------------------------------------------------------------
/**
 *  @brief Face recognition with LDA and BG model.
 *  @author Jose M. Buenaposada, Pablo Marín
 *  @date 2012/4
 *  @version $revision$
 *
 *  $id$
 *
 *  Grupo de investigación en Percepción Computacional y Robótica)
 *  (Perception for Computers & Robots research Group)
 *  Facultad de Informática (Computer Science School)
 *  Universidad Politécnica de Madrid (UPM) (Madrid Technical University)
 *  http://www.dia.fi.upm.es/~pcr
 *
 */
// -----------------------------------------------------------------------------

// ------------------ RECURSION PROTECTION -------------------------------------
#ifndef FACE_ID_LDA_BGMODEL_PROCESSOR_HPP
#define FACE_ID_LDA_BGMODEL_PROCESSOR_HPP

// ----------------------- INCLUDES --------------------------------------------
#include "face_processor.hpp"
#include "face_id_database.hpp"
#include "face_background_model.hpp"
#include <opencv/cv.h>
#include <opencv/ml.h>
#include <set>

namespace upm { namespace pcr
{

// ----------------------------------------------------------------------------
/**
 * @class FaceIdLDABgModelProcessor
 * @brief An image region processor subclass for face recognition
 * 
 * In this class we use LDA projection of the 64x64 input images. The LDA 
 * projection is trained with PIE database subjects as classes. So LDA 
 * would allow to separate face images according with they similarity to 
 * PIE subjects. After LDA projection we compute the minimum euclidean 
 * distance with all the PIE subjects images (projections) and we compute 
 * a vector with the similarity of the input image to each of the PIE subjects. 
 * We use the index (instead of the distance) of the PIE subject in the ordeder
 * list of distances to the input face image. Therefore we use the PIE database
 * as the Background Model (or library) of identities to compare with. The
 * vector of indices of similarity is our high level descriptor of the input 
 * image. 
 */
static float MINIMAL_NUMBER_OF_IMAGES = 20;

// -----------------------------------------------------------------------------
class FaceIdLDABgModelProcessor: public FaceProcessor
{
public:

  FaceIdLDABgModelProcessor
    (
    FaceBackgroundModelPtr face_bg_model,
    FaceIdDataBasePtr face_id_db
    );

  /** Copy constructor */
  FaceIdLDABgModelProcessor
    (
    const FaceIdLDABgModelProcessor& processor
    );

  /** Asignment operator */
  FaceIdLDABgModelProcessor&
  operator =
    (
    const   FaceIdLDABgModelProcessor& processor
    );

  ~FaceIdLDABgModelProcessor
    ();

  /**
   * @brief Process face region for recognition
   *
   * @param pFrame Image comming from the camera.
   * @param f The face within pFrame to process.
   */
  void
  processFrame 
   (
   IplImage* pFrame,
   Face& f 
   );

  /**
   * @brief Shows the recognition results (Face ID)
   *
   * @param pViewer The viewer over to which display results.
   * @param pFrame Last processed camera frame.
   * @param f The face within pFrame that was used in the last call to processFrame.
   */
  void
  showResults
   (
   Viewer* pViewer,
   IplImage* pFrame,
   Face& f
   );

  /**
   * @brief Returns a pointer to a copy of this object.
   */
  FaceProcessorPtr
  clone
   ();

  /**
   * @brief Get the face attributes computed by the face region processor.
   *
   * @param attributes
   *
   * An example of the kind of attributes that this region processor generates
   * is:
   *     "face_id" ATTR_STRING_TYPE "value1" 0.95
   *
   * In this case the confidence is equal to the probability of the identification.
   *
   */
  void
  getAttributes
    (
    FaceAttributes& attributes
    );

private:
  cv::Mat m_image;
  cv::Mat m_P;
  cv::Mat m_mean;
  
  /** The face images descriptors seen so far **/
  std::queue<cv::Mat> m_face_descriptors_seen;  
  
  float m_face_id_confidence;
  FaceIdDataBase::id_type m_face_id;
  FaceIdDataBasePtr m_face_id_db;
  FaceBackgroundModelPtr m_face_bg_model; 
};

}; }; // namespace

#endif

