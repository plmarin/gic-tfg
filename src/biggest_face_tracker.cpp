// -----------------------------------------------------------------------------
/**
 *  @brief BiggestFaceTracker implementation
 *  @author Jose M. Buenaposada
 *  @date 2009/13/10
 *  @version $revision$
 *
 *  $id$
 *
 *  Grupo de investigación en Percepción Computacional y Robótica)
 *  (Perception for Computers & Robots research Group)
 *  Facultad de Informática (Computer Science School)
 *  Universidad Politécnica de Madrid (UPM) (Madrid Technical University)
 *  http://www.dia.fi.upm.es/~pcr
 *
 */
// -----------------------------------------------------------------------------

// ----------------------- INCLUDES --------------------------------------------
#include "biggest_face_tracker.hpp"
#include <algorithm> // std::copy
#include <limits>
#include "trace.hpp"

namespace upm { namespace pcr
{

// -----------------------------------------------------------------------------
//
// Purpose and Method: Constructor
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
BiggestFaceTracker::BiggestFaceTracker
  (
  CvHaarClassifierCascade* pCascade,
  CvHaarClassifierCascade* pCascadeLeftEye,
  CvHaarClassifierCascade* pCascadeRightEye,
  double scale_step,
  double min_neighbors,
  double min_face_size
  ):
  m_scale_step(scale_step),
  m_min_neighbors(min_neighbors),
  m_min_face_size(min_face_size),
  m_eyes_detector(pCascadeLeftEye, pCascadeRightEye, pCascade->orig_window_size.width),
  m_num_faces_created(0)
{
  if (pCascade != NULL)
  {
    m_pStorage = cvCreateMemStorage(0);
    m_pCascade = pCascade;
  }
 
};

// -----------------------------------------------------------------------------
//
// Purpose and Method: Destructor
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
BiggestFaceTracker::~BiggestFaceTracker
  ()
{
  if (m_pStorage) 
  {
    cvReleaseMemStorage(&m_pStorage);
  }

  if (m_pCascade) 
  {
    cvReleaseHaarClassifierCascade(&m_pCascade);
  };
};

// -----------------------------------------------------------------------------
//
// Purpose and Method: 
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
void 
BiggestFaceTracker::processFrame 
  (
  IplImage *pImage,
  Faces& tracked_faces
  )
{
  IplImage* pGrayImage;
  IplImage* pAuxImage;
  CvSeq* faces;
  CvPoint left_eye, right_eye;
  int max_width = 0;
  int max_height = 0;
  int nearest_face_index = -1;
  static float alpha = 0.4;
  static float alpha1 = 1.0 - alpha;
//  int old_x, old_y, old_width, old_height;
  double ratio_height, ratio_width, ratio_x, ratio_y;
  double min_ratio_height, min_ratio_width, min_ratio_x, min_ratio_y;
  int x, y, width, height;
//  int x_roi, y_roi, width_roi, height_roi;
  ImageRegion region;

  if ((!m_pCascade) || (!m_pStorage))
  {
    return;
  };
  
  // Obtain a grey levels version of the input image 
  pGrayImage = cvCreateImage( cvSize(pImage->width, pImage->height), 8, 1 );
  if ( pImage->origin == IPL_ORIGIN_TL )
  {
    cvCvtColor( pImage, pGrayImage, CV_BGR2GRAY );
  }
  else
  {
    pAuxImage  = cvCreateImage( cvSize(pImage->width, pImage->height), 8, 1 );
    cvCvtColor( pImage, pAuxImage, CV_BGR2GRAY );
    cvFlip( pAuxImage, pGrayImage, 0 );
  }

  // Prepare the face detection memory storage and detect faces
  cvClearMemStorage( m_pStorage );

  faces = cvHaarDetectObjects(pGrayImage, m_pCascade, m_pStorage,
                              m_scale_step,
                              m_min_neighbors, 
                              CV_HAAR_DO_CANNY_PRUNING | CV_HAAR_SCALE_IMAGE,
                              //CV_HAAR_DO_CANNY_PRUNING,
                              cvSize(m_min_face_size, m_min_face_size));
  cvReleaseImage(&pGrayImage);
  
  if (faces->total > 0)
  {
    // We have found at least one face 
    if (tracked_faces.empty())
    {
      // Look for the biggest face the first time
      max_height = std::numeric_limits<int>::min();
      max_width  = std::numeric_limits<int>::min();
      nearest_face_index = -1;
      //SHOW_VALUE(faces->total)
      for( int i = 0; i < (faces ? faces->total : 0); i++ )
      {
        CvRect* r = (CvRect*)cvGetSeqElem( faces, i );

        if ( (r->height>max_height) && (r->width>max_width) )
        {
          max_width  = r->width;
          max_height = r->height;
          nearest_face_index = i;
        }
      };
      
      CvRect* r = (CvRect*)cvGetSeqElem( faces, nearest_face_index );
      region.setParams(r->x, r->y, r->width, r->height);
           
      // We have detected first time a face (and we choose the biggest). 
      boost::shared_ptr<Face> face_ptr(new Face(++m_num_faces_created));
      face_ptr->setRegion(region);      
      tracked_faces.push_back(face_ptr);      
    }    
    else
    {
      // Update biggest face on image position and size.
      ImageRegion old_region;
      CvRect r_nearest;

      // Compute the weighted average of the current face 
      // image region params and the former (old) image
      // ones. The parameter alpha (between 0.0 and 1.0)
      // controls the weight of the last image and the current.
      tracked_faces[0]->getRegion(old_region);
      old_region.getParams(x, 
                           y, 
                           width, 
                           height);      

      // Look for the closest face to the tracked one
      min_ratio_height = std::numeric_limits<int>::max();
      min_ratio_width  = std::numeric_limits<int>::max();
      min_ratio_x      = std::numeric_limits<int>::max();
      min_ratio_y      = std::numeric_limits<int>::max();      
      for( int i = 0; i < (faces ? faces->total : 0); i++ )
      {
        CvRect* r = (CvRect*)cvGetSeqElem( faces, i );
	ratio_height = static_cast<double>(abs(r->height-height))/static_cast<double>(height);
	ratio_width  = static_cast<double>(abs(r->width-width))/static_cast<double>(width);
	ratio_x      = static_cast<double>(abs(r->x-x))/static_cast<double>(x);
        ratio_y      = static_cast<double>(abs(r->y-y))/static_cast<double>(y);
        if ( (ratio_width  < min_ratio_width) && 
             (ratio_height < min_ratio_height)  && 
             (ratio_x      < min_ratio_x)  &&
             (ratio_y      < min_ratio_y) )
        {
          r_nearest        = (*r);
          min_ratio_height = ratio_height;
          min_ratio_width  = ratio_width;
          min_ratio_x      = ratio_x;
          min_ratio_y      = ratio_y;
        }
      };

      region.setParams(static_cast<int>((alpha*static_cast<double>(x)) +
                                        (alpha1*static_cast<double>(r_nearest.x))), 
                       static_cast<int>((alpha*static_cast<double>(y)) +
                                        (alpha1*static_cast<double>(r_nearest.y))), 
                       static_cast<int>((alpha*static_cast<double>(width)) + 
                                        (alpha1*static_cast<double>(r_nearest.width))), 
                       static_cast<int>((alpha*static_cast<double>(height)) + 
                                        (alpha1*static_cast<double>(r_nearest.height))));
      tracked_faces[0]->setRegion(region);
    };

    // We find here the eyes of the detected face
    m_eyes_detector.detect(pImage, region, left_eye, right_eye);
    tracked_faces[0]->setEyesRelativeCoordinates(left_eye, right_eye);
  }
  else
  {
    // We have not found any face
    tracked_faces.clear();
  }
};

// -----------------------------------------------------------------------------
//
// Purpose and Method: 
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
void 
BiggestFaceTracker::showResults
  (
  Viewer* pViewer,
  IplImage *pImage,
  Faces& tracked_faces
  )
{
  int x, y;
  int width, height;
  CvPoint left_eye, right_eye;
  float red[3]   = {1.0, 0.0, 0.0};
  float green[3] = {0.0, 1.0, 0.0};
  float blue[3]  = {0.0, 0.0, 1.0};
  float white[3] = {1.0, 1.0, 1.0};

  // Show detected faces.
  for (int i=0; i<tracked_faces.size(); i++)
  {
    ImageRegion region;
    tracked_faces[i]->getRegion(region); 
    region.getParams(x, y, width, height);
    pViewer->rectangle(x, 
                       y,
                       width,
                       height,
                       2,
                       white);
    tracked_faces[i]->getEyesRelativeCoordinates(left_eye, right_eye);
    m_eyes_detector.showResults(pViewer, pImage, region, left_eye, right_eye);
  };
};

// -----------------------------------------------------------------------------
//
// Purpose and Method: 
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
/*
void
BiggestFaceTracker::getTrackedFaces
  (
  upm::pcr::Faces& tracked_faces
  )
{
  tracked_faces.resize(m_tracked_faces.size());
  std::copy(m_tracked_faces.begin(), m_tracked_faces.end(), tracked_faces.begin());
};
*/

}; }; // namespace
