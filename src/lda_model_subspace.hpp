// -----------------------------------------------------------------------------
/**
 *  @brief ...
 *  @author Jose M. Buenaposada, Pablo Marín
 *  @date 2013/5
 *  @version $revision$
 *
 *  $id$
 *
 *  Grupo de investigación en Percepción Computacional y Robótica)
 *  (Perception for Computers & Robots research Group)
 *  Facultad de Informática (Computer Science School)
 *  Universidad Politécnica de Madrid (UPM) (Madrid Technical University)
 *  http://www.dia.fi.upm.es/~pcr
 *
 */
// -----------------------------------------------------------------------------

// ------------------ RECURSION PROTECTION -------------------------------------
#ifndef LDA_MODEL_SUBSPACE_HPP
#define LDA_MODEL_SUBSPACE_HPP

// ----------------------- INCLUDES --------------------------------------------
#include <opencv/cv.h>
#include <opencv/ml.h>
#include <boost/shared_ptr.hpp>

namespace upm { namespace pcr
{

class LDAModelSubspace;

typedef boost::shared_ptr<LDAModelSubspace> LDAModelSubspacePtr;
// ----------------------------------------------------------------------------
/**
 * @class LDAModelSubspace
 * @brief 
 * 
 */
// -----------------------------------------------------------------------------
  
class LDAModelSubspace
{
public:
  
  LDAModelSubspace
    ();
    
  ~LDAModelSubspace
    ();
  
  cv::Mat
  computeLowLevelDescriptor
    (
    cv::Mat image
    );
  
    
private:
  
  cv::Mat m_P;
  cv::Mat m_mean;
    
};

}; }; // namespace





#endif // LDA_MODEL_SUBSPACE_HPP
