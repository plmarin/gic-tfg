
// -----------------------------------------------------------------------------
/**
 *  @brief Implementation of DCT features extractor by blocks.
 *  @author Jose M. Buenaposada, Pablo Marín
 *  @date 2013/5
 *  @version $revision$
 *
 *  $id$
 *
 *  Grupo de investigación en Percepción Computacional y Robótica)
 *  (Perception for Computers & Robots research Group)
 *  Facultad de Informática (Computer Science School)
 *  Universidad Politécnica de Madrid (UPM) (Madrid Technical University)
 *  http://www.dia.fi.upm.es/~pcr
 *
 */
// -----------------------------------------------------------------------------

#include "dct_blocks_descriptor.hpp"
#include "similarity_warp_filter.hpp"
#include <opencv/cv.h>
#include <cmath>
#include "trace.hpp"
#include <boost/concept_check.hpp>

namespace upm { namespace pcr
{
  
#define FACE_SIZE 64
#define BLOCK_SIZE 8
#define SCALE (static_cast<double>(FACE_SIZE)/25.0)
#define NORM_LEFT_EYE_X cvRound(5.0*SCALE)
#define NORM_LEFT_EYE_Y cvRound(7.0*SCALE)
#define NORM_RIGHT_EYE_X cvRound(18.0*SCALE)
#define NORM_RIGHT_EYE_Y cvRound(7.0*SCALE)  
  
// -----------------------------------------------------------------------------
//
// Purpose and Method:  
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
DCTBlocksDescriptor::DCTBlocksDescriptor
  (): m_STEP(4)
{
  
}

// -----------------------------------------------------------------------------
//
// Purpose and Method:  Copy constructor
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
DCTBlocksDescriptor::DCTBlocksDescriptor
  ( 
  const DCTBlocksDescriptor& descriptor
  ): m_STEP(4)
{
  (*this) = (descriptor);
}

// -----------------------------------------------------------------------------
//
// Purpose and Method:  Asignment operator.
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
DCTBlocksDescriptor& 
DCTBlocksDescriptor::operator= 
  ( 
  const DCTBlocksDescriptor& descriptor
  )
{
  if (&descriptor == this)
  {
    return (*this);
  };
  
  return (*this);
}

// -----------------------------------------------------------------------------
//
// Purpose and Method:  
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
DCTBlocksDescriptor::~DCTBlocksDescriptor
  ()
{

}

// -----------------------------------------------------------------------------
//
// Purpose and Method: 
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
DCTBlocksDescriptorPtr
DCTBlocksDescriptor::clone
  ()
{
  DCTBlocksDescriptorPtr descriptor_ptr(new DCTBlocksDescriptor((*this)));

  return descriptor_ptr;
}

// -----------------------------------------------------------------------------
//
// Purpose and Method: Extract DCT features by blocks
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
cv::Mat 
DCTBlocksDescriptor::computeHighLevelDescriptor
  (
  cv::Mat low_level_descriptor
  )
{
  cv::Mat pFeatures = cv::Mat(1125, 1, cv::DataType<float>::type);
  cv::Mat pDCTImage;
  cv::Mat pGrayImage;
  cv::Mat pGrayImageF;
  int i, j;
  int ind;
  double value;
  float *row;
  double dct_values[5];
  double dct_norm;
  int zigzag_x[5] = {1, 0, 0, 1, 2};
  int zigzag_y[5] = {0, 1, 2, 1, 0};
  
  assert(! pFeatures.empty());
  assert(! low_level_descriptor.empty());
  assert(pFeatures.cols == 1);
  //assert(pFeatures.rows == 5 * pow(cvRound(static_cast<double>(FACE_SIZE) / BLOCK_SIZE), 2));

  pDCTImage = cv::Mat(BLOCK_SIZE, BLOCK_SIZE, CV_32FC1);
  pGrayImageF = cv::Mat(low_level_descriptor.cols, low_level_descriptor.rows, low_level_descriptor.type());
  pGrayImage = cv::Mat(low_level_descriptor.cols, low_level_descriptor.rows, low_level_descriptor.depth());

  if(low_level_descriptor.channels() == 3)
  {
    cv::cvtColor(low_level_descriptor, pGrayImage, CV_RGB2GRAY);
  }
  else
  {
    pGrayImage = low_level_descriptor;
  }

  pGrayImage.convertTo(pGrayImage, CV_32FC1);
  
  ind = 0;
  for (int y = 0; y + BLOCK_SIZE - 1 < low_level_descriptor.rows; y += this->m_STEP)
  {
    for (int x = 0; x + BLOCK_SIZE - 1 < low_level_descriptor.cols; x += this->m_STEP)
    {      
      cv::Rect rect;
      rect.x = x;
      rect.y = y;
      rect.width = BLOCK_SIZE;
      rect.height = BLOCK_SIZE;
      
      pGrayImageF = cv::Mat(pGrayImage, rect);
      cv::dct(pGrayImageF, pDCTImage, 0);

      dct_norm = 0;

      // We ignore the first value and put the next 5 
      // values in zig-zag on the pDCTFeatures vector.
      for (int i = 0; i < 5; i++)
      {
        row = pDCTImage.ptr<float>(zigzag_y[i]);
        dct_values[i] = row[zigzag_x[i]];
        dct_norm += pow(row[zigzag_x[i]], 2);
      } 

      dct_norm = sqrt(dct_norm);

      for (int i = 0; i < 5; i++)
      { 
	if (dct_norm != 0.0)
	{
          pFeatures.row(ind++).col(0) = dct_values[i] / dct_norm;
	}
	else
	{
          pFeatures.row(ind++).col(0) = dct_values[i];
	}
      }
    }
  }
  
  return pFeatures;
}

}; }; // namespace




