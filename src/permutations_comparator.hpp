// -----------------------------------------------------------------------------
/**
 *  @brief ...
 *  @author Jose M. Buenaposada, Pablo Marín
 *  @date 2013/5
 *  @version $revision$
 *
 *  $id$
 *
 *  Grupo de investigación en Percepción Computacional y Robótica)
 *  (Perception for Computers & Robots research Group)
 *  Facultad de Informática (Computer Science School)
 *  Universidad Politécnica de Madrid (UPM) (Madrid Technical University)
 *  http://www.dia.fi.upm.es/~pcr
 *
 */
// -----------------------------------------------------------------------------

// ------------------ RECURSION PROTECTION -------------------------------------
#ifndef PERMUTATIONS_COMPARATOR_HPP
#define PERMUTATIONS_COMPARATOR_HPP

// ----------------------- INCLUDES --------------------------------------------
#include "descriptor_comparator.hpp"


namespace upm { namespace pcr
{

// ----------------------------------------------------------------------------
/**
 * @class PermutationsComparator
 * @brief ...
 */
// -----------------------------------------------------------------------------
  
class PermutationsComparator : public DescriptorComparator
{
public:
  
  PermutationsComparator
    ();
  
  virtual
  ~PermutationsComparator
    ();
    
  /**
   * @brief Compare two descriptors
   *
   * @param new_descriptor
   * @param descriptor
   */
  virtual double
  compare
    (
    cv::Mat new_descriptor,
    cv::Mat descriptor
    );  
  
  /**
   * @brief
   *
   * @param
   */
  virtual bool
  isDistance
    ();
  
   /**
   * @brief
   *
   * @param
   */
  virtual bool
  isSimilarity
    ();  
    
};

}; }; // namespace


#endif // PERMUTATIONS_COMPARATOR_HPP
