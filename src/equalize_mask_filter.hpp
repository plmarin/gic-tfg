// -----------------------------------------------------------------------------
/**
 *  @brief Equalization+Mask image region filter interface definition
 *  @author Jose M. Buenaposada
 *  @date 2010/12/02 
 *  @version $revision$
 *
 *  $id$
 *
 *  Grupo de investigación en Percepción Computacional y Robótica)
 *  (Perception for Computers & Robots research Group)
 *  Facultad de Informática (Computer Science School)
 *  Universidad Politécnica de Madrid (UPM) (Madrid Technical University)
 *  http://www.dia.fi.upm.es/~pcr
 *
 */
// -----------------------------------------------------------------------------

// ------------------ RECURSION PROTECTION -------------------------------------
#ifndef EQUALIZE_MASK_FILTER_HPP
#define EQUALIZE_MASK_FILTER_HPP

// ----------------------- INCLUDES --------------------------------------------
#include "face_area_image_filter.hpp"
#include <boost/shared_ptr.hpp>
#include <opencv/cv.h>
#include <opencv/ml.h>

namespace upm { namespace pcr
{
// ----------------------------------------------------------------------------
/**
 *  Definition of an smart pointer to EqualizeMaskFilter type
 */
// -----------------------------------------------------------------------------
class EqualizeMaskFilter;
typedef boost::shared_ptr<EqualizeMaskFilter> EqualizeMaskFilterPtr;

// ----------------------------------------------------------------------------
/**
 * @class EqualizeMaskFilter
 * @brief An image region filter for equalization and masking
 */
// -----------------------------------------------------------------------------
class EqualizeMaskFilter: public FaceAreaImageFilter
{
public:

  EqualizeMaskFilter
    (
    CvMat* pMask,
    int img_width,
    int img_height,
    bool use_eyes_location = false,
    bool use_equalization = true
    );

  /** Copy constructor */
  EqualizeMaskFilter
    (
    const EqualizeMaskFilter& filter
    );

  /** Asignment operator */
  EqualizeMaskFilter&
  operator =
    (
    const EqualizeMaskFilter& filter
    );

  ~EqualizeMaskFilter
    ();

  EqualizeMaskFilterPtr
  clone
    ();
    
  /**
   * @brief Crops the face region from the input image.
   *
   * @param pFrame Image comming from the camera.
   * @param f The face within pFrame to process.
   * @param pFilteredImage The output of the procedure
   */
  virtual void
  operator () 
   (
   IplImage* pFrame,
   Face& f,
   IplImage* pFilteredImage
   );
   
private:

  /**
   * @brief Put the mask over the input face image
   *
   * @param pImage NxN pixels input/output grey level image (N=m_cropped_image_size).
   * @param pMask A NxN matrix (N=m_cropped_image_size)
   */
  void
  putMask
   (
   IplImage* pImage,
   CvMat* pMask
   );

  // Image region mask
  CvMat*  m_pMask; 
  int m_img_width;
  int m_img_height;
  bool m_use_eyes_location;
  bool m_use_equalization;
  CvPoint m_norm_left_eye;
  CvPoint m_norm_right_eye;
};

}; }; // namespace

#endif
