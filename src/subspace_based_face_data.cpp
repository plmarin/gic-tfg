// -----------------------------------------------------------------------------
/**
 *  @brief ...
 *  @author Jose M. Buenaposada, Pablo Marín
 *  @date 2013/5
 *  @version $revision$
 *
 *  $id$
 *
 *  Grupo de investigación en Percepción Computacional y Robótica)
 *  (Perception for Computers & Robots research Group)
 *  Facultad de Informática (Computer Science School)
 *  Universidad Politécnica de Madrid (UPM) (Madrid Technical University)
 *  http://www.dia.fi.upm.es/~pcr
 *
 */
// -----------------------------------------------------------------------------

#include "subspace_based_face_data.hpp"

namespace upm { namespace pcr
{
  
// -----------------------------------------------------------------------------
//
// Purpose and Method:  
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
SubspaceBasedFaceData::SubspaceBasedFaceData
  ()
{
}

// -----------------------------------------------------------------------------
//
// Purpose and Method:  
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
SubspaceBasedFaceData::~SubspaceBasedFaceData
  ()
{
}

// -----------------------------------------------------------------------------
//
// Purpose and Method:  
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
cv::Mat
SubspaceBasedFaceData::compare
  (
  //SubspaceBasedFaceData& new_descriptors,
  //SubspaceBasedFaceData& db_descriptors
  FaceDataPtr new_descriptors,
  FaceDataPtr db_descriptors
  )
{
  
}
 
// -----------------------------------------------------------------------------
//
// Purpose and Method:  
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// ----------------------------------------------------------------------------- 
void
SubspaceBasedFaceData::addImage
  (
  cv::Mat image
  )
{
  
}

// -----------------------------------------------------------------------------
//
// Purpose and Method:  
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// ----------------------------------------------------------------------------- 
bool 
SubspaceBasedFaceData::isOk
  ()
{

}

}; }; // namespace
